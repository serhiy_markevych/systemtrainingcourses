﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CommonClassesLib;
using CommonClassesLib.DataModels;
using CommonClassesLib.Services;
using CommonClassesLib.Exceptions;

namespace AuthenticationLib
{
    public static class Authentication
    {
        #region Fields
        private static UserDataModel _currentUser = null;
        #endregion

        #region Property
        public static UserDataModel CurrentUser
        {
            get { return Authentication._currentUser; }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Method of user authentication. Created object current user.
        /// </summary>
        /// <param name="username">username</param>
        /// <param name="password">user password</param>
        /// <returns>DataModel current user</returns>
        public static UserDataModel Login(string username, string password)
        {
            try
            {
                if (Validation.ValidUsername(username) && Validation.ValidPassword(password))
                {
                    UserService userService = UserService.GetInstance();

                    UserDataModel tempUser = userService.GetUser(username);

                    if (tempUser != null)
                    {
                        if (Authentication.HashPassword(password) == tempUser.HashedPassword)
                            _currentUser = tempUser;
                    }

                    return _currentUser;
                }
                else
                {
                    return _currentUser;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Dispose current user object.
        /// </summary>
        /// <returns>Bool</returns>
        public static bool Logout()
        {
            try
            {
                _currentUser = null;

                return true;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Check the role of the user id.
        /// </summary>
        /// <param name="userId">user id</param>
        /// <param name="roleName">role name</param>
        /// <returns>Bool</returns>
        public static bool HasRole(int userId, Role roleName)
        {
            try
            {
                bool result = false;

                UserService userService = UserService.GetInstance();

                UserDataModel tempUser = userService.GetUser(userId);

                if (tempUser != null)
                {
                    if (tempUser.HasRole(roleName))
                        result = true;
                }

                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Check the role of the user name.
        /// </summary>
        /// <param name="username">username</param>
        /// <param name="roleName">role name</param>
        /// <returns>Bool</returns>
        public static bool HasRole(string username, Role roleName)
        {
            try
            {
                bool result = false;

                UserService userService = UserService.GetInstance();

                UserDataModel tempUser = userService.GetUser(username);

                if (tempUser != null)
                {
                    if (tempUser.HasRole(roleName))
                        result = true;
                }

                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Implements password hashing.
        /// </summary>
        /// <param name="password">password</param>
        /// <returns>String of bytes</returns>
        public static string HashPassword(string password)
        {
            try
            {
                byte[] bytePassword = Encoding.UTF8.GetBytes(password);

                string result = "";

                foreach (byte item in bytePassword)
                    result += item;

                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion
    }
}