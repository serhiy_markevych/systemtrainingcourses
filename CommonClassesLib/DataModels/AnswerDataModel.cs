﻿using System;
using System.Xml.Serialization;

namespace CommonClassesLib.DataModels
{
    /// <summary>
    /// Record in a table "Answers"
    /// </summary>
    [XmlType("Answer")]
    public class AnswerDataModel : IEquatable<AnswerDataModel>
    {
        [XmlAttribute("ID")]
        public int ID { get; set; }
        [XmlElement("Questions_ID")]
        public int QuestionsID { get; set; }
        [XmlAttribute("Ordinal_number")]
        public int OrdinalNumber { get; set; }
        [XmlAttribute("Canceled_mark")]
        public int CanceledMark { get; set; }
        public string Text { get; set; }
        public int Significance { get; set; }
        public string Group { get; set; }

        #region Constructors

        public AnswerDataModel()
        { }


        public AnswerDataModel(int id, int questionsID, int ordinalNumber = 1, string text = "", int significance = 0, string group = "", int canceledMark = 0)
        {
            ID = id;
            QuestionsID = questionsID;
            OrdinalNumber = ordinalNumber;
            Text = text;
            Significance = significance;
            Group = group;
            CanceledMark = canceledMark;
        }

        public AnswerDataModel(int questions_id, int ordinal_number, string text, int significance, string group, int canceled_mark)
        {
            this.ID = 0;
            this.QuestionsID = questions_id;
            this.OrdinalNumber = ordinal_number;
            this.Text = text;
            this.Significance = significance;
            this.Group = group;
            this.CanceledMark = canceled_mark;
        }

        #endregion

        public override string ToString()
        {
            return Text;
        }

        #region IEquatable implementation

        public bool Equals(AnswerDataModel other)
        {
            if (other == null)
                return false;

            return this.ID.Equals(other.ID) && this.QuestionsID.Equals(other.QuestionsID);
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as AnswerDataModel);
        }

        public override int GetHashCode()
        {
            return string.Format("{0}{1}", ID, QuestionsID).GetHashCode();
        }

        public static bool operator ==(AnswerDataModel operandA, AnswerDataModel operandB)
        {
            return Equals(operandA, operandB);
        }

        public static bool operator !=(AnswerDataModel operandA, AnswerDataModel operandB)
        {
            return !Equals(operandA, operandB);
        }

        #endregion
    }
}
