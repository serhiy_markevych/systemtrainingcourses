﻿using System;
using System.Xml.Serialization;

namespace CommonClassesLib.DataModels
{
    /// <summary>
    /// Record in a table "Assign"
    /// </summary>
    [XmlType("Assign")]
    public class AssignDataModel: IEquatable<AssignDataModel>
    {
        [XmlAttribute("ID")]
        public int ID { get; set; }
        [XmlElement("Subjects_ID")]
        public int SubjectsID { get; set; }
        [XmlElement("Groups_ID")]
        public int GroupsID { get; set; }
        [XmlElement("Teacher_ID")]
        public int TeacherID { get; set; }
        [XmlElement("Profiles_ID")]
        public int ProfilesID { get; set; }

        #region Constructors

        public AssignDataModel()
        { }

        public AssignDataModel(int id, int subjectsID, int groupsID, int teacherID, int profilesID)
        {
            ID = id;
            SubjectsID = subjectsID;
            GroupsID = groupsID;
            TeacherID = teacherID;
            ProfilesID = profilesID;
        }

        public AssignDataModel(int subjects_id, int groups_id, int teacher_id, int profiles_id) 
        {
            this.SubjectsID = subjects_id;
            this.GroupsID = groups_id;
            this.TeacherID = teacher_id;
            this.ProfilesID = profiles_id;
        }

        #endregion

        #region IEquatable implementation

        public bool Equals(AssignDataModel other)
        {
            if (other == null)
                return false;

            return this.SubjectsID.Equals(other.SubjectsID) && this.GroupsID.Equals(other.GroupsID);
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as AssignDataModel);
        }

        public override int GetHashCode()
        {
            return string.Format("{0}{1}", SubjectsID, GroupsID).GetHashCode();
        }

        public static bool operator ==(AssignDataModel operandA, AssignDataModel operandB)
        {
            return Equals(operandA, operandB);
        }

        public static bool operator !=(AssignDataModel operandA, AssignDataModel operandB)
        {
            return !Equals(operandA, operandB);
        }

        #endregion
    }
}
