﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Xml;
using CommonResourcesLib;

namespace CommonClassesLib.DataModels
{
    [Serializable, XmlRoot("Group"), XmlType("Group")]
    public class GroupDataModel
    {
        #region Fields
        private int _id;
        private string _name;
        private bool _canceledMark;
        #endregion

        #region Constructors
        public GroupDataModel(string name, bool canceledMark)
        {
            _name = name;
            _canceledMark = canceledMark;
        }
        public GroupDataModel()
        {
            _name = "";
            _canceledMark = false;
        }
        #endregion

        #region Methods
        public override string ToString()
        {
            return String.Format(Messages.GroupToString, _name);
        }
        #endregion

        #region Properties
        [XmlAttribute]
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }
        [XmlAttribute]
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }
        [XmlAttribute]
        public bool CanceledMark
        {
            get { return _canceledMark; }
            set { _canceledMark = value; }
        }
        #endregion

    }
}
