﻿using System;
using System.Xml.Serialization;

namespace CommonClassesLib.DataModels
{
    /// <summary>
    /// Record in a table "Moduls"
    /// </summary>
    [XmlType("Modul")]
    public class ModulDataModel: IEquatable<ModulDataModel>
    {
        [XmlAttribute("ID")]
        public int ID { get; set; }
        [XmlElement("Name")]
        public string Name { get; set; }
        [XmlAttribute("Canceled_mark")]
        public int CanceledMark { get; set; }
        [XmlElement("Subjects_ID")]
        public int SubjectsID { get; set; }

        #region Constructors

        public ModulDataModel()
        { }

        public ModulDataModel(int id, int subjectsID, string name = "", int canceledMark = 0)
        {
            ID = id;
            SubjectsID = subjectsID;
            Name = name;
            CanceledMark = canceledMark;
        }

        public ModulDataModel(string name, int canceled_mark, int subjects_id)
        {
            this.Name = name;
            this.CanceledMark = canceled_mark;
            this.SubjectsID = subjects_id;
        }

        public ModulDataModel(int id, string name, int canceled_mark, int subjects_id)
        {
            this.ID = id;
            this.Name = name;
            this.CanceledMark = canceled_mark;
            this.SubjectsID = subjects_id;
        }

        #endregion

        #region IEquatable implementation

        public bool Equals(ModulDataModel other)
        {
            if (other == null)
                return false;

            return this.ID.Equals(other.ID);
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as ModulDataModel);
        }

        public override int GetHashCode()
        {
            return ID;
        }

        public static bool operator ==(ModulDataModel operandA, ModulDataModel operandB)
        {
            return Equals(operandA, operandB);
        }

        public static bool operator !=(ModulDataModel operandA, ModulDataModel operandB)
        {
            return !Equals(operandA, operandB);
        }

        #endregion
    }
}
