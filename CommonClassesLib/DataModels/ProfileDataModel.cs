﻿using System;
using System.Xml.Serialization;

namespace CommonClassesLib.DataModels
{
    /// <summary>
    /// Record in a table "Profiles"
    /// </summary>
    [XmlType("Profile")]
    public class ProfileDataModel:IEquatable<ProfileDataModel>
    {
        [XmlAttribute("ID")]
        public int ID { get; set; }
        public string Name { get; set; }
        [XmlAttribute("Canceled_mark")]
        public int CanceledMark { get; set; }
        [XmlElement("Time_limit")]
        public int TimeLimit { get; set; }
        [XmlElement("Shuffle_questions")]
        public int ShuffleQuestions { get; set; }
        [XmlElement("Shuffle_answers")]
        public int ShuffleAnswers { get; set; }
        [XmlElement("Questions_count")]
        public int QuestionsCount { get; set; }
        [XmlElement("Moduls_ID")]
        public int? ModulsID { get; set; }

        #region Constructors

        public ProfileDataModel()
        { }

        public ProfileDataModel(int id, string name = "", int canceledMark = 0, int timeLimit = 0, int shuffleQuestions = 0, int shuffleAnswers = 0,
            int questionsCount = 0, int? modulsID = null)
        {
            ID = id;
            Name = name;
            CanceledMark = canceledMark;
            TimeLimit = timeLimit;
            ShuffleAnswers = shuffleAnswers;
            ShuffleQuestions = shuffleQuestions;
            QuestionsCount = questionsCount;
            ModulsID = modulsID;
        }

        public ProfileDataModel(string name, int canceled_mark, int time_limit, int shuffle_questions, int shuffle_answers, int questions_count, int moduls_id)
        {
            this.Name = name;
            this.CanceledMark = canceled_mark;
            this.TimeLimit = time_limit;
            this.ShuffleQuestions = shuffle_questions;
            this.ShuffleAnswers = shuffle_answers;
            this.QuestionsCount = questions_count;
            this.ModulsID = moduls_id;
        }

        public ProfileDataModel(int id, string name, int canceled_mark, int time_limit, int shuffle_questions, int shuffle_answers, int questions_count, int moduls_id)
        {
            this.ID = id;
            this.Name = name;
            this.CanceledMark = canceled_mark;
            this.TimeLimit = time_limit;
            this.ShuffleQuestions = shuffle_questions;
            this.ShuffleAnswers = shuffle_answers;
            this.QuestionsCount = questions_count;
            this.ModulsID = moduls_id;
        }

        #endregion

        #region IEquatable inplementation

        public bool Equals(ProfileDataModel other)
        {
            if (other == null)
                return false;

            return this.ID.Equals(other.ID);
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as ProfileDataModel);
        }

        public override int GetHashCode()
        {
            return ID;
        }

        public static bool operator ==(ProfileDataModel operandA, ProfileDataModel operandB)
        {
            return Equals(operandA, operandB);
        }

        public static bool operator !=(ProfileDataModel operandA, ProfileDataModel operandB)
        {
            return !Equals(operandA, operandB);
        }

        #endregion
    }
}
