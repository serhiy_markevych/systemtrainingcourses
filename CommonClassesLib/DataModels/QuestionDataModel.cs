﻿using System;
using System.Xml.Serialization;

namespace CommonClassesLib.DataModels
{
    /// <summary>
    /// Record in a table "Questions"
    /// </summary>
    [XmlType("Question")]
    public class QuestionDataModel : IEquatable<QuestionDataModel>
    {
        [XmlAttribute("ID")]
        public int ID { get; set; }
        [XmlAttribute("Ordinal_number")]
        public int OrdinalNumber { get; set; }
        [XmlAttribute("Canceled_mark")]
        public int CanceledMark { get; set; }
        public string Text { get; set; }
        public int Significance { get; set; }
        public QuestionType Type { get; set; }
        [XmlElement("Moduls_ID")]
        public int ModulsID { get; set; }

        #region Constructors

        public QuestionDataModel()
        { }

        public QuestionDataModel(int id, int ordinalNumber = 1, string text = "", int significance = 1, QuestionType type = 0, int modulsID = 0, int canceledMark = 0)
        {
            ID = id;
            OrdinalNumber = ordinalNumber;
            Text = text;
            Significance = significance;
            Type = type;
            ModulsID = modulsID;
            CanceledMark = canceledMark;
        }

        public QuestionDataModel(int ordinal_number, string text, int significance, QuestionType type, int moduls_id, int canceled_mark)
        {
            this.OrdinalNumber = ordinal_number;
            this.Text = text;
            this.Significance = significance;
            this.Type = type;
            this.ModulsID = moduls_id;
            this.CanceledMark = canceled_mark;
        }

        #endregion

        public override string ToString()
        {
            return string.Format("Question {0}{1}{2}", OrdinalNumber, Environment.NewLine, Text);
        }

        #region IEquatable implementation

        public bool Equals(QuestionDataModel other)
        {
            if (other == null)
                return false;

            return this.ID.Equals(other.ID);
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as QuestionDataModel);
        }

        public override int GetHashCode()
        {
            return ID;
        }

        public static bool operator ==(QuestionDataModel operandA, QuestionDataModel operandB)
        {
            return Equals(operandA, operandB);
        }

        public static bool operator !=(QuestionDataModel operandA, QuestionDataModel operandB)
        {
            return Equals(operandA, operandB);
        }

        #endregion
    }
}
