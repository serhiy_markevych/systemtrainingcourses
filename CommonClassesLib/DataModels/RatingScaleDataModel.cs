﻿using System;
using System.Xml.Serialization;

namespace CommonClassesLib.DataModels
{
    /// <summary>
    /// Record in a table "Rating_scale"
    /// </summary>
    [XmlType("Rating_scale")]
    public class RatingScaleDataModel : IEquatable<RatingScaleDataModel>
    {
        [XmlAttribute("ID")]
        public int ID { get; set; }
        [XmlElement("Profiles_ID")]
        public int ProfilesID { get; set; }
        public string Text { get; set; }
        [XmlElement("Low_bound")]
        public int LowBound { get; set; }

        #region Constructors

        public RatingScaleDataModel()
        { }

        public RatingScaleDataModel(int id, int profilesID, string text = "", int lowBound = 0)
        {
            ID = id;
            ProfilesID = profilesID;
            Text = text;
            LowBound = lowBound;
        }

        public RatingScaleDataModel(int profilesId, string text, int lowBound)
        {
            this.ProfilesID = profilesId;
            this.Text = text;
            this.LowBound = lowBound;
        }

        #endregion

        #region IEquatable implementation

        public bool Equals(RatingScaleDataModel other)
        {
            if (other == null)
                return false;

            return this.ID.Equals(other.ID);
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as RatingScaleDataModel);
        }

        public override int GetHashCode()
        {
            return ID;
        }

        public static bool operator ==(RatingScaleDataModel operandA, RatingScaleDataModel operandB)
        {
            return Equals(operandA, operandB);
        }

        public static bool operator !=(RatingScaleDataModel operandA, RatingScaleDataModel operandB)
        {
            return !Equals(operandA, operandB);
        }

        #endregion
    }
}
