﻿using System;
using System.Xml.Serialization;

namespace CommonClassesLib.DataModels
{
    /// <summary>
    /// Record in a table "Subjects"
    /// </summary>
    [XmlType("Subject")]
    public class SubjectDataModel : IEquatable<SubjectDataModel>
    {
        [XmlAttribute("ID")]
        public int ID { get; set; }
        public string Name { get; set; }
        [XmlAttribute("Canceled_mark")]
        public int CanceledMark { get; set; }

        #region Constructors

        public SubjectDataModel()
        { }

        public SubjectDataModel(int id, string name = "", int canceledMark = 0)
        {
            ID = id;
            Name = name;
            CanceledMark = canceledMark;
        }

        public SubjectDataModel(string name, int canceled_mark)
        {
            this.Name = name;
            this.CanceledMark = canceled_mark;
        }

        #endregion

        #region IEquatable implementation

        public bool Equals(SubjectDataModel other)
        {
            if (other == null)
                return false;

            return this.ID.Equals(other.ID);
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as SubjectDataModel);
        }

        public override int GetHashCode()
        {
            return ID;
        }

        public static bool operator ==(SubjectDataModel operandA, SubjectDataModel operandB)
        {
            return Equals(operandA, operandB);
        }

        public static bool operator !=(SubjectDataModel operandA, SubjectDataModel operandB)
        {
            return !Equals(operandA, operandB);
        }

        #endregion
    }
}
