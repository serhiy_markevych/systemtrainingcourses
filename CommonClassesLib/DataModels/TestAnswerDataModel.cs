﻿using System;
using System.Xml.Serialization;

namespace CommonClassesLib.DataModels
{
    /// <summary>
    /// Record in a table "Test_answers"
    /// </summary>
    [XmlType("Test_answer")]
    public class TestAnswerDataModel : IEquatable<TestAnswerDataModel>
    {
        [XmlAttribute("ID")]
        public int ID { get; set; }
        [XmlElement("Test_questions_ID")]
        public int TestQuestionsID { get; set; }
        [XmlElement("Answers_ID")]
        public int AnswersID { get; set; }
        [XmlAttribute("Ordinal_number")]
        public int OrdinalNumber { get; set; }
        [XmlElement("Text")]
        public string Text { get; set; }

        #region Constructors

        public TestAnswerDataModel()
        { }

        public TestAnswerDataModel(int id, int testQuestionsID, int answersID, int ordinalNumber = 1, string text = "")
        {
            ID = id;
            TestQuestionsID = testQuestionsID;
            AnswersID = answersID;
            OrdinalNumber = ordinalNumber;
            Text = text;
        }

        #endregion

        #region IEquatable implementation

        public bool Equals(TestAnswerDataModel other)
        {
            if (other == null)
                return false;

            return this.ID.Equals(other.ID);
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as TestAnswerDataModel);
        }

        public override int GetHashCode()
        {
            return ID;
        }

        public static bool operator ==(TestAnswerDataModel operandA, TestAnswerDataModel operandB)
        {
            return Equals(operandA, operandB);
        }

        public static bool operator !=(TestAnswerDataModel operandA, TestAnswerDataModel operandB)
        {
            return !Equals(operandA, operandB);
        }

        #endregion
    }
}
