﻿using System;
using System.Xml.Serialization;

namespace CommonClassesLib.DataModels
{
    /// <summary>
    /// Record in a table "Tests"
    /// </summary>
    [XmlType("Test")]
    public class TestDataModel : IEquatable<TestDataModel>
    {
        private TimeSpan _duration;

        [XmlAttribute("ID")]
        public int ID { get; set; }
        [XmlElement("Subjects_ID")]
        public int SubjectsID { get; set; }
        [XmlElement("Student_ID")]
        public int StudentID { get; set; }
        [XmlElement("Teacher_ID")]
        public int TeacherID { get; set; }
        [XmlElement("Profiles_ID")]
        public int ProfilesID { get; set; }
        public TestMode Mode { get; set; }
        public DateTime Date { get; set; }
        [XmlIgnore]
        public TimeSpan Duration
        {
            get { return _duration; }
            set { _duration = value; }
        }
        [XmlElementAttribute("Duration")]
        public long DurationTicks
        {
            get { return _duration.Ticks; }
            set { _duration = new TimeSpan(value); }
        }
        public int Rating { get; set; }
        [XmlElement("Total_rating")]
        public int TotalRating { get; set; }
        [XmlElement("Total_questions")]
        public int TotalQuestions { get; set; }
        [XmlElement("Total_answers")]
        public int TotalAnswers { get; set; }

        #region Constructors

        public TestDataModel()
        { }

        public TestDataModel(int id, int subjectsID, int studentID, int teacherID, int profilesID, TestMode mode, DateTime date, TimeSpan duration, int rating = 0, 
            int totalRating = 0, int totalQuestions = 0, int totalAnswers = 0)
        {
            ID = id;
            SubjectsID = subjectsID;
            StudentID = studentID;
            TeacherID = teacherID;
            ProfilesID = profilesID;
            Mode = mode;
            Date = date;
            Duration = duration;
            Rating = rating;
            TotalRating = totalRating;
            TotalQuestions = totalQuestions;
            TotalAnswers = totalAnswers;
        }

        #endregion

        #region IEquatable implementation

        public bool Equals(TestDataModel other)
        {
            if (other == null)
                return false;

            return this.ID.Equals(other.ID);
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as TestDataModel);
        }

        public override int GetHashCode()
        {
            return ID;
        }

        public static bool operator ==(TestDataModel operandA, TestDataModel operandB)
        {
            return Equals(operandA, operandB);
        }

        public static bool operator !=(TestDataModel operandA, TestDataModel operandB)
        {
            return !Equals(operandA, operandB);
        }

        #endregion
    }
}
