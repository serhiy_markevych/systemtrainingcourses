﻿using System;
using System.Xml.Serialization;

namespace CommonClassesLib.DataModels
{
    /// <summary>
    /// Record in a table "Test_questions"
    /// </summary>
    [XmlType("TestQuestion")]
    public class TestQuestionDataModel : IEquatable<TestQuestionDataModel>
    {
        [XmlAttribute("ID")]
        public int ID { get; set; }
        [XmlElement("Tests_ID")]
        public int TestsID { get; set; }
        [XmlElement("Questions_ID")]
        public int QuestionsID { get; set; }
        [XmlAttribute("Ordinal_number")]
        public int OrdinalNumber { get; set; }

        #region Constructors

        public TestQuestionDataModel()
        { }

        public TestQuestionDataModel(int id, int testsID, int questionsID, int ordinalNumber = 1)
        {
            ID = id;
            TestsID = testsID;
            QuestionsID = questionsID;
            OrdinalNumber = ordinalNumber;
        }

        #endregion

        #region IEquatable implementation

        public bool Equals(TestQuestionDataModel other)
        {
            if (other == null)
                return false;

            return this.ID.Equals(other.ID);
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as TestQuestionDataModel);
        }

        public override int GetHashCode()
        {
            return ID;
        }

        public static bool operator ==(TestQuestionDataModel operandA, TestQuestionDataModel operandB)
        {
            return Equals(operandA, operandB);
        }

        public static bool operator !=(TestQuestionDataModel operandA, TestQuestionDataModel operandB)
        {
            return !Equals(operandA, operandB);
        }

        #endregion
    }
}
