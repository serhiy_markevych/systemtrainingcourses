﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Xml;
using CommonClassesLib.Interfaces;
using CommonResourcesLib;

namespace CommonClassesLib.DataModels
{
    [Serializable, XmlRoot("User"), XmlType("User")]
    public class UserDataModel : IUser
    {
        #region Fields
        private int _id;
        private string _username;
        private string _email;
        private string _hashedPassword;
        private List<Role> _roles;
        private GroupDataModel _group;
        private int _groupId;
        private string _firstName;
        private string _lastName;
        private string _middleName;
        #endregion

        #region Constructors
        public UserDataModel()
        {
            _username = "";
            _hashedPassword = "";
            _email = "";
            _roles = new List<Role>();
            _groupId = -1;
            _group = new GroupDataModel();
            _firstName = "";
            _lastName = "";
            _middleName = "";
        }
        public UserDataModel(
            string username,
            string email,
            string hashedPassword,
            List<Role> roles,
            GroupDataModel group = null,
            string firstName = "",
            string lastName = "",
            string middleName = "")
        {
            _username = username;
            _email = email;
            _hashedPassword = hashedPassword;
            _roles = roles;
            _groupId = -1;
            _group = group;
            _firstName = firstName;
            _lastName = lastName;
            _middleName = middleName;

        }
        #endregion

        #region Methods
        /// <summary>
        /// Checking if user has specified role
        /// </summary>
        /// <param name="role">role to check</param>        
        /// <returns>bool</returns>
        public bool HasRole(Role role)
        {
            try
            {
                return _roles.Contains(role);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public override string ToString()
        {
            string roles = "";
            foreach (Role role in _roles)
            {
                roles += role.ToString() + " ";
            }
            return string.Format(Messages.UserToString, _username, _email, _id, roles);
        }
        #endregion

        #region Properties
        [XmlAttribute]
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }
        [XmlElement]
        public string Username
        {
            get { return _username; }
            set { _username = value; }
        }
        [XmlElement]
        public string Email
        {
            get { return _email; }
            set { _email = value; }
        }
        [XmlElement]
        public string HashedPassword
        {
            get { return _hashedPassword; }
            set { _hashedPassword = value; }
        }
        [XmlArray]
        public List<Role> Roles
        {
            get { return _roles; }
            set { _roles = value; }
        }
        [XmlIgnore]
        public GroupDataModel Group
        {
            get { return _group; }
            set { _group = value; }
        }
        [XmlElement]
        public int GroupId
        {
            get { return _groupId; }
            set { _groupId = value; }
        }
        [XmlAttribute]
        public string FirstName
        {
            get { return _firstName; }
            set { _firstName = value; }
        }
        [XmlAttribute]
        public string LastName
        {
            get { return _lastName; }
            set { _lastName = value; }
        }
        [XmlAttribute]
        public string MiddleName
        {
            get { return _middleName; }
            set { _middleName = value; }
        }
        #endregion
    }
}
