﻿namespace CommonClassesLib
{
    public enum Role : byte
    {
        Student = 0,
        Teacher = 1
    }

    /// <summary>
    /// Enumeration of the test modes
    /// </summary>
    public enum TestMode
    {
        /// <summary>
        /// Learning mode
        /// </summary>
        Learning = 0,
        /// <summary>
        /// Testing mode
        /// </summary>
        Testing = 1
    }

    /// <summary>
    /// Enumeration of question types
    /// </summary>
    public enum QuestionType
    {
        /// <summary>
        /// Question with one correct answer
        /// </summary>
        OneAnswer = 0,
        /// <summary>
        /// Multiple choice question
        /// </summary>
        MultipleAnswers = 1,
        /// <summary>
        /// Question with typing answer
        /// </summary>
        TypeAnswer = 2,
        /// <summary>
        /// Question to establish compliance //соответствие
        /// </summary>
        Compliance = 3,
        /// <summary>
        /// Question on the ordering //в правильной последовательности
        /// </summary>
        Ordering = 4,
        /// <summary>
        /// Question on the classification //по группам
        /// </summary>
        Classification = 5
    }
}
