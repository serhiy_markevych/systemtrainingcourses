﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonClassesLib.Exceptions
{
    [Serializable]
    public class TrainingException : ApplicationException
    {
        #region Constructors
        public TrainingException()
        {

        }

        public TrainingException(string message)
            : base(message)
        {

        }

        public TrainingException(string message, System.Exception inner)
            : base(message, inner)
        {

        }

        protected TrainingException(System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContext context)
            : base(info, context)
        {

        }
        #endregion 
    }
}
