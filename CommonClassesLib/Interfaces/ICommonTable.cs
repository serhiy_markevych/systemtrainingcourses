﻿using System.Linq;

namespace CommonClassesLib.Interfaces
{
    public interface ICommonTable<T>
    {
        void DeleteData(T item);
        IQueryable <T> GetData();
        void InsertData(T item);
        bool ExistsData(T item);
        void SaveData();
    }
}
