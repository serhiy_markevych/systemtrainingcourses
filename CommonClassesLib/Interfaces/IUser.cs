﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonClassesLib.Interfaces
{
    public interface IUser
    {
        int Id { get; set; }
        string Username { get; set; }
        string HashedPassword { get; set; }
        List<Role> Roles { get; set; }
    }
}
