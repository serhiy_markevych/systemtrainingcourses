﻿using System;
using System.Linq;
using CommonClassesLib.DataModels;
using CommonClassesLib.Interfaces;
using CommonClassesLib.Tools;

namespace CommonClassesLib.Services
{
    /// <summary>
    /// "Answers" table services
    /// </summary>
    public sealed class AnswersService : CommonService<AnswerDataModel>
    {
        private volatile static AnswersService _uniqueInstance = null;
        private static string _tableName = "Answers";

        private AnswersService(ICommonTable<AnswerDataModel> commonTable)
            : base(commonTable)
        { }

        /// <summary>
        /// Get instance of the "AnswersService" class
        /// </summary>
        /// <returns>"AnswersService" class unique object</returns>
        public static AnswersService GetInstance()
        {
            if (_uniqueInstance == null)
                lock (_tableName)
                    if (_uniqueInstance == null)
                        _uniqueInstance = new AnswersService(CommonTableFactory.CreateCommonTable<AnswerDataModel>(_tableName));
            return _uniqueInstance;
        }

        /// <summary>
        /// Get records of the table by field "Questions_ID"
        /// </summary>
        /// <param name="questionsID">"Questions_ID" field value</param>
        /// <returns>Records that match the search condition</returns>
        public IQueryable<AnswerDataModel> GetRecordsByQuestionsID(int questionsID)
        {
            return
                from i in GetRecords()
                where i.QuestionsID == questionsID
                select i;
        }

        /// <summary>
        /// Get records of the table by field "Сanceled_mark"
        /// </summary>
        /// <param name="canceledMark">"Сanceled_mark" field value</param>
        /// <returns>Records that match the search condition</returns>
        public IQueryable<AnswerDataModel> GetRecordsByCanceledMark(int canceledMark = 0)
        {
            return
                from i in GetRecords()
                where i.CanceledMark == canceledMark
                select i;
        }

        /// <summary>
        /// Get list of Answers
        /// </summary>
        public int GetMaxID()
{
            int iddata = 0;
            try
            {
                iddata = (from aitem in GetRecords()
                              orderby aitem.ID descending
                              select aitem.ID).FirstOrDefault<int>();
                iddata += 1;
            }
            catch(Exception ex)
            {
                Log.ErrorLog(ex, "Невозможно определить новый id записи");
            }
            return iddata;
        }

        /// <summary>
        /// Add Answers data
        /// </summary>
        /// <param name="record">New object of Answers</param>
        public void AddData(AnswerDataModel record)
        {
            record.ID = GetMaxID();
            base.InsertRecord(record);
        }

        /// <summary>
        /// Update Answers data
        /// </summary>
        /// <param name="idoldobj">ID value of Answers old object</param>
        /// <param name="newobj">Updated object of Answers</param>
        public void EditData(int idoldobj, AnswerDataModel newobj)
        {
            var edit = GetRecords().Where(o => o.ID == idoldobj).FirstOrDefault();

            if (edit != null && newobj != null)
            {
                edit.ID = newobj.ID;
                edit.QuestionsID = newobj.QuestionsID;
                edit.OrdinalNumber = newobj.OrdinalNumber;
                edit.Text = newobj.Text;
                edit.Significance = newobj.Significance;
                edit.Group = newobj.Group;
                edit.CanceledMark = newobj.CanceledMark;
            }
        }

        /// <summary>
        /// Remove Answers data
        /// </summary>
        /// <param name="obj">Removing object of Answers</param>
        public void RemoveData(int id)
        {
            var record = GetRecords().Where(o => o.ID == id).FirstOrDefault();
            if(record != null)
                DeleteRecord(record);
        }
    }
}
