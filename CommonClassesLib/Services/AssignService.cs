﻿using System.Linq;
using CommonClassesLib.DataModels;
using CommonClassesLib.Interfaces;
using CommonClassesLib.Tools;
using System;

namespace CommonClassesLib.Services
{
    /// <summary>
    /// "Assign" table services
    /// </summary>
    public sealed class AssignService: CommonService<AssignDataModel>
    {
        private volatile static AssignService _uniqueInstance = null;
        private static string _tableName = "Assign";

        private AssignService(ICommonTable<AssignDataModel> commonTable)
            : base(commonTable)
        { }

        /// <summary>
        /// Get instance of the "AssignService" class
        /// </summary>
        /// <returns>"AssignService" class unique object</returns>
        public static AssignService GetInstance()
        {
            if (_uniqueInstance == null)
                lock (_tableName)
                    if (_uniqueInstance == null)
                        _uniqueInstance = new AssignService(CommonTableFactory.CreateCommonTable<AssignDataModel>(_tableName));
            return _uniqueInstance;
        }

        /// <summary>
        /// Get record of the table by fields "Subjects_ID" и "Groups_ID"
        /// </summary>
        /// <param name="subjectsID">"Subjects_ID" field value</param>
        /// <param name="groupsID">"Groups_ID" field value</param>
        /// <returns>Record that matches the search condition</returns>
        public AssignDataModel GetRecordBySubjectsIDGroupsID(int subjectsID, int groupsID)
        {
            return
                (from a in GetRecords()
                 where a.SubjectsID == subjectsID && a.GroupsID == groupsID
                 select a).FirstOrDefault();
        }

        /// <summary>
        /// Get list of Assign
        /// </summary>
        public int GetMaxID()
        {
            int iddata = 0;
            try
            {
                iddata = (from aitem in GetRecords()
                          orderby aitem.ID descending
                          select aitem.ID).FirstOrDefault<int>();
                iddata += 1;
            }
            catch (Exception ex)
            {
                Log.ErrorLog(ex, "Невозможно определить новый id записи");
            }
            return iddata;
        }

        /// <summary>
        /// Add Assign data
        /// </summary>
        /// <param name="record">New object of Assign</param>
        public void AddData(AssignDataModel record)
        {
            record.ID = GetMaxID();
            base.InsertRecord(record);
        }

        /// <summary>
        /// Update Assign data
        /// </summary>
        /// <param name="idoldobj">ID value of Assign old object</param>
        /// <param name="newobj">Updated object of Assign</param>
        public void EditData(int idoldobj, AssignDataModel newobj)
        {
            var edit = GetRecords().Where(o => o.ID == idoldobj).FirstOrDefault();

            if (edit != null && newobj != null)
            {
                edit.ID = newobj.ID;
                edit.SubjectsID = newobj.SubjectsID;
                edit.GroupsID = newobj.GroupsID;
                edit.TeacherID = newobj.TeacherID;
                edit.ProfilesID = newobj.ProfilesID;
            }
        }

        /// <summary>
        /// Remove Answers data
        /// </summary>
        /// <param name="obj">Removing object of Answers</param>
        public void RemoveData(int id)
        {
            var record = GetRecords().Where(o => o.ID == id).FirstOrDefault();
            if (record != null)
                DeleteRecord(record);
        }
    }
}
