﻿using System;
using System.Collections.Generic;
using System.Linq;
using CommonClassesLib.Interfaces;

namespace CommonClassesLib.Services
{
    /// <summary>
    /// Common table services
    /// </summary>
    /// <typeparam name="T">Data type that represents a table record</typeparam>
    public abstract class CommonService<T>
    {
        private ICommonTable<T> _context;

        protected CommonService(ICommonTable<T> commonTable)
        {
            if (commonTable == null)
                throw new ArgumentNullException();

            _context = commonTable;
        }

        /// <summary>
        /// Removes record from a table
        /// </summary>
        /// <param name="record">Record that must be removed</param>
        public void DeleteRecord(T record)
        {
            _context.DeleteData(record);
        }

        /// <summary>
        /// Retrieves records from a table
        /// </summary>
        /// <returns>All records of a table</returns>
        public IQueryable<T> GetRecords()
        {
            return _context.GetData();
        }

        /// <summary>
        /// Get list of records
        /// </summary>
        public List<T> GetData()
        {
            return GetRecords().ToList();
        }

        /// <summary>
        /// Get list of records
        /// </summary>
        /// <param name="field">Field of table which will go sample</param>
        /// <param name="value">Value for sample</param>
        public List<T> GetData(string field, string value)
        {
            List<T> data = (from aitem in GetData()
                                          where aitem.GetType().GetProperty(field).GetValue(aitem, null).Equals(value)
                                          select aitem).ToList();
            return data;
        }

        /// <summary>
        /// Get list of records
        /// </summary>
        /// <param name="field">Field of table which will go sample</param>
        /// <param name="value">Value for sample</param>
        public List<T> GetData(string field, int value)
        {
            List<T> data = (from aitem in GetData()
                                          where (int)aitem.GetType().GetProperty(field).GetValue(aitem, null) == value
                                          select aitem).ToList();
            return data;
        }

        /// <summary>
        /// Adds record to a table
        /// </summary>
        /// <param name="record">Record that must be inserted</param>
        public virtual void InsertRecord(T record)
        {
            _context.InsertData(record);
        }

        /// <summary>
        /// Determines whether the record is in a table
        /// </summary>
        /// <param name="record">The record to locate in a table</param>
        /// <returns>true if record is found in a table; otherwise, false</returns>
        public bool IsRecord(T record)
        {
            return _context.ExistsData(record);
        }

        /// <summary>
        /// Persists all updates to the data source
        /// </summary>
        public void SubminChanges()
        {
            _context.SaveData();
        }
    }
}
