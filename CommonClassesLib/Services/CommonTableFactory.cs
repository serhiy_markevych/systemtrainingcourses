﻿using System.Configuration;
using System.IO;
using CommonClassesLib.Interfaces;

namespace CommonClassesLib.Services
{
    sealed class CommonTableFactory
    {
        public static ICommonTable<T> CreateCommonTable<T>(string tableName)
        {
            return new SerializeCommonTable<T>(Path.Combine(ConfigurationManager.AppSettings["xmlDatabasePath"], tableName.Trim() + ".xml"));
        }
    }
}
