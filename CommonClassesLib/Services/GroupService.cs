﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CommonClassesLib.Services;
using CommonClassesLib.Exceptions;
using CommonClassesLib.DataModels;
using CommonClassesLib.Interfaces;
using CommonResourcesLib;

namespace CommonClassesLib.Services
{
    public sealed class GroupService : CommonService<GroupDataModel>
    {
        #region Fields
        private volatile static GroupService _uniqueInstance = null;
        private static string _tableName = "Groups";
        #endregion


        #region Constructors
        private GroupService(ICommonTable<GroupDataModel> commonTable)
            : base(commonTable)
        { }
        #endregion


        #region Methods
        /// <summary>
        /// Get a reference to the class object AnswersService
        /// </summary>
        /// <returns>Unique object class AnswersService</returns>
        public static GroupService GetInstance()
        {
            if (_uniqueInstance == null)
                lock (_tableName)
                    if (_uniqueInstance == null)
                        _uniqueInstance = new GroupService(CommonTableFactory.CreateCommonTable<GroupDataModel>(_tableName));
            return _uniqueInstance;
        }

        /// <summary>
        /// Method will return group with specified id
        /// </summary>
        /// <param name="id">group id</param>        
        /// <returns>GroupDataModel group</returns>
        public GroupDataModel GetGroup(int id)
        {
            var groups = from g in GetRecords()
                         where g.Id == id
                         select g;

            GroupDataModel group = groups.FirstOrDefault();

            return group;
        }

        /// <summary>
        /// Method will return group with specified name
        /// </summary>
        /// <param name="name">group name</param>
        /// <returns>GroupDataModel group</returns>
        public GroupDataModel GetGroup(string groupName)
        {
            var groups = from g in GetRecords()
                         where g.Name == groupName
                         select g;

            GroupDataModel group = groups.FirstOrDefault();

            return group;
        }

        /// <summary>
        /// Get all groups from database
        /// </summary>        
        /// <returns>collection of group</returns>
        public List<GroupDataModel> GetGroups()
        {
            GroupService gService = GroupService.GetInstance();

            return GetRecords().ToList<GroupDataModel>();
        }

        /// <summary>
        /// Add new group to database
        /// </summary>
        /// <param name="group">group object</param>        
        /// <returns>void</returns>
        public void AddGroup(GroupDataModel group)
        {
            if (this.IsUnique(this.GetGroups(), group))
            {
                group.Id = this.GetNewId();
                this.InsertRecord(group);
                this.SubminChanges();
            }
            else
            {
                throw new TrainingException(Errors.GroupAlreadyExist);
            }
        }

        /// <summary>
        /// Get new ID for group
        /// </summary>
        /// <returns></returns>
        public int GetNewId()
        {
            if (GetRecords().Count() > 0)
                return
                    (from t in GetRecords()
                     select t.Id).Max() + 1;
            else
                return 0;
        }

        /// <summary>
        /// Check if same group already exist in collection
        /// </summary>
        /// <param name="groups">group collection in which will check</param>
        /// <param name="groupToCheck">group to check</param>
        /// <returns>bool</returns>
        public bool IsUnique(List<GroupDataModel> groups, GroupDataModel groupToCheck)
        {
            // try select from collection group with same data that in groupToCheck
            var result = from g in groups
                         where g.Name == groupToCheck.Name
                         select g;

            GroupDataModel group = result.FirstOrDefault();

            // if group exist than groupToCheck in not unique
            if (group != null) return false;
            else return true;
        }

        /// <summary>
        /// Delete group by id
        /// </summary>
        /// <param name="name">group id</param>        
        public void DeleteGroup(int id)
        {
            GroupDataModel groupToDelete = this.GetGroup(id);

            if (groupToDelete != null)
            {
                base.DeleteRecord(groupToDelete);
                base.SubminChanges();
            }
        }
        #endregion
    }
}