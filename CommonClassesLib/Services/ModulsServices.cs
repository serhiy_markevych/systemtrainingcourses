﻿using System.Collections.Generic;
using System.Linq;
using CommonClassesLib.DataModels;
using CommonClassesLib.Interfaces;
using System;
using CommonClassesLib.Tools;

namespace CommonClassesLib.Services
{
    /// <summary>
    /// "Moduls" table services
    /// </summary>
    public sealed class ModulsService: CommonService<ModulDataModel>
    {
        private volatile static ModulsService _uniqueInstance = null;
        private static string _tableName = "Moduls";

        private ModulsService(ICommonTable<ModulDataModel> commonTable)
            : base(commonTable)
        { }

        /// <summary>
        /// Get instance of the "ModulsService" class
        /// </summary>
        /// <returns>"ModulsService" class unique object</returns>
        public static ModulsService GetInstance()
        {
            if (_uniqueInstance == null)
                lock (_tableName)
                    if (_uniqueInstance == null)
                        _uniqueInstance = new ModulsService(CommonTableFactory.CreateCommonTable<ModulDataModel>(_tableName));
            return _uniqueInstance;
        }

        /// <summary>
        /// Get records of the table by field "Subjects_ID"
        /// </summary>
        /// <param name="subjectsID">"Subjects_ID" field value</param>
        /// <returns>Records that match the search condition</returns>
        public IQueryable<ModulDataModel> GetRecordsBySubjectsID(int subjectsID)
        {
            return
                from m in GetRecords()
                where m.SubjectsID == subjectsID
                select m;
        }

        /// <summary>
        /// Get record of the table by field "ID"
        /// </summary>
        /// <param name="id">"ID" field value</param>
        /// <returns>Record that matches the search condition</returns>
        public ModulDataModel GetRecordByID(int id)
        {
            return
                (from m in GetRecords()
                 where m.ID == id
                 select m).FirstOrDefault();
        }

        /// <summary>
        /// Get list of Moduls
        /// </summary>
        public int GetMaxID()
        {
            int iddata = 0;
            try
            {
                iddata = (from aitem in GetRecords()
                          orderby aitem.ID descending
                          select aitem.ID).FirstOrDefault<int>();
                iddata += 1;
            }
            catch (Exception ex)
            {
                Log.ErrorLog(ex, "Невозможно определить новый id записи");
            }
            return iddata;
        }

        /// <summary>
        /// Add Moduls data
        /// </summary>
        /// <param name="record">New object of Moduls</param>
        public void AddData(ModulDataModel record)
        {
            record.ID = GetMaxID();
            base.InsertRecord(record);
        }

        /// <summary>
        /// Update Moduls data
        /// </summary>
        /// <param name="idoldobj">ID value of Moduls old object</param>
        /// <param name="newobj">Updated object of Moduls</param>
        public void EditData(int idoldobj, ModulDataModel newobj)
        {
            var edit = GetRecords().Where(o => o.ID == idoldobj).FirstOrDefault();

            if (edit != null && newobj != null)
            {
                edit.ID = newobj.ID;
                edit.Name = newobj.Name;
                edit.CanceledMark = newobj.CanceledMark;
                edit.SubjectsID = newobj.SubjectsID;
            }
        }

        /// <summary>
        /// Remove Answers data
        /// </summary>
        /// <param name="obj">Removing object of Answers</param>
        public void RemoveData(int id)
        {
            var record = GetRecords().Where(o => o.ID == id).FirstOrDefault();
            if (record != null)
                DeleteRecord(record);
        }
    }
}
