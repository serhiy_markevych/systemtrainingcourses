﻿using System.Linq;
using CommonClassesLib.DataModels;
using CommonClassesLib.Interfaces;
using System;
using CommonClassesLib.Tools;

namespace CommonClassesLib.Services
{
    /// <summary>
    /// "Profiles" table services
    /// </summary>
    public sealed class ProfilesService: CommonService<ProfileDataModel>
    {
        private volatile static ProfilesService _uniqueInstance = null;
        private static string _tableName = "Profiles";

        private ProfilesService(ICommonTable<ProfileDataModel> commonTable)
            : base(commonTable)
        { }

        /// <summary>
        /// Get instance of the "ProfilesService" class
        /// </summary>
        /// <returns>"ProfilesService" class unique object</returns>
        public static ProfilesService GetInstance()
        {
            if (_uniqueInstance == null)
                lock (_tableName)
                    if (_uniqueInstance == null)
                        _uniqueInstance = new ProfilesService(CommonTableFactory.CreateCommonTable<ProfileDataModel>(_tableName));
            return _uniqueInstance;
        }

        /// <summary>
        /// Get record of the table by field "ID"
        /// </summary>
        /// <param name="id">"ID" field value</param>
        /// <returns>Record that matches the search condition</returns>
        public ProfileDataModel GetRecordByID(int id)
        {
            return
                (from p in GetRecords()
                where p.ID == id
                select p).FirstOrDefault();
        }

        /// <summary>
        /// Get list of Profiles
        /// </summary>
        public int GetMaxID()
        {
            int iddata = 0;
            try
            {
                iddata = (from aitem in GetRecords()
                          orderby aitem.ID descending
                          select aitem.ID).FirstOrDefault<int>();
                iddata += 1;
            }
            catch (Exception ex)
            {
                Log.ErrorLog(ex, "Невозможно определить новый id записи");
            }
            return iddata;
        }

        /// <summary>
        /// Add Profiles data
        /// </summary>
        /// <param name="record">New object of Profiles</param>
        public void AddData(ProfileDataModel record)
        {
            record.ID = GetMaxID();
            base.InsertRecord(record);
        }

        /// <summary>
        /// Update Profiles data
        /// </summary>
        /// <param name="idoldobj">ID value of Profiles old object</param>
        /// <param name="newobj">Updated object of Profiles</param>
        public void EditData(int idoldobj, ProfileDataModel newobj)
        {
            var edit = GetRecords().Where(o => o.ID == idoldobj).FirstOrDefault();

            if (edit != null && newobj != null)
            {
                edit.ID = newobj.ID;
                edit.Name = newobj.Name;
                edit.CanceledMark = newobj.CanceledMark;
                edit.TimeLimit = newobj.TimeLimit;
                edit.ShuffleQuestions = newobj.ShuffleQuestions;
                edit.ShuffleAnswers = newobj.ShuffleAnswers;
                edit.QuestionsCount = newobj.QuestionsCount;
                edit.ModulsID = newobj.ModulsID;
            }
        }

        /// <summary>
        /// Remove Answers data
        /// </summary>
        /// <param name="obj">Removing object of Answers</param>
        public void RemoveData(int id)
        {
            var record = GetRecords().Where(o => o.ID == id).FirstOrDefault();
            if (record != null)
                DeleteRecord(record);
        }
    }
}
