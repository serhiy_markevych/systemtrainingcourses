﻿using System.Linq;
using CommonClassesLib.DataModels;
using CommonClassesLib.Interfaces;
using System;
using CommonClassesLib.Tools;

namespace CommonClassesLib.Services
{
    /// <summary>
    /// "Questions" table services
    /// </summary>
    public sealed class QuestionsService : CommonService<QuestionDataModel>
    {
        private volatile static QuestionsService _uniqueInstance = null;
        private static string _tableName = "Questions";

        private QuestionsService(ICommonTable<QuestionDataModel> commonTable)
            : base(commonTable)
        { }

        /// <summary>
        /// Get instance of the "QuestionsService" class
        /// </summary>
        /// <returns>"QuestionsService" class unique object</returns>
        public static QuestionsService GetInstance()
        {
            if (_uniqueInstance == null)
                lock (_tableName)
                    if (_uniqueInstance == null)
                        _uniqueInstance = new QuestionsService(CommonTableFactory.CreateCommonTable<QuestionDataModel>(_tableName));
            return _uniqueInstance;
        }

        /// <summary>
        /// Get record of the table by field "ID"
        /// </summary>
        /// <param name="id">"ID" field value</param>
        /// <returns>Record that matches the search condition</returns>
        public QuestionDataModel GetRecordByID(int id)
        {
            return
                (from q in GetRecords()
                 where q.ID == id
                 select q).FirstOrDefault();
        }

        /// <summary>
        /// Get max ID
        /// </summary>
        public int GetMaxID()
        {
            int iddata = 0;
            try
            {
                iddata = (from aitem in GetRecords()
                          orderby aitem.ID descending
                          select aitem.ID).FirstOrDefault<int>();
                iddata += 1;
            }
            catch (Exception ex)
            {
                Log.ErrorLog(ex, "Невозможно определить новый id записи");
            }
            return iddata;
        }

        /// <summary>
        /// Add Questions data
        /// </summary>
        /// <param name="record">New object of Questions</param>
        public void AddData(QuestionDataModel record)
        {
            record.ID = GetMaxID();
            base.InsertRecord(record);
        }

        /// <summary>
        /// Update Questions data
        /// </summary>
        /// <param name="idoldobj">ID value of Questions old object</param>
        /// <param name="newobj">Updated object of Questions</param>
        public void EditData(int idoldobj, QuestionDataModel newobj)
        {
            var edit = GetRecords().Where(o => o.ID == idoldobj).FirstOrDefault();

            if (edit != null && newobj != null)
            {
                edit.ID = newobj.ID;
                edit.OrdinalNumber = newobj.OrdinalNumber;
                edit.Text = newobj.Text;
                edit.Significance = newobj.Significance;
                edit.Type = newobj.Type;
                edit.ModulsID = newobj.ModulsID;
                edit.CanceledMark = newobj.CanceledMark;
            }
        }

        /// <summary>
        /// Remove Questions data
        /// </summary>
        /// <param name="obj">Removing object of Questions</param>
        public void RemoveData(int id)
        {
            var record = GetRecords().Where(o => o.ID == id).FirstOrDefault();
            try
            {
                if (record.ID > 0)
                    DeleteRecord(record);
            }
            catch (NullReferenceException)
            {
                Console.WriteLine("Не верно указан id вопроса");
            }
        }
    }
}
