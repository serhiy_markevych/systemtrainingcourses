﻿using System.Collections.Generic;
using System.Linq;
using CommonClassesLib.DataModels;
using CommonClassesLib.Interfaces;
using System;
using CommonClassesLib.Tools;

namespace CommonClassesLib.Services
{
    /// <summary>
    /// "Rating_scale" table services
    /// </summary>
    public sealed class RatingScaleService: CommonService<RatingScaleDataModel>
    {
        private volatile static RatingScaleService _uniqueInstance = null;
        private static string _tableName = "Rating_scale";

        private RatingScaleService(ICommonTable<RatingScaleDataModel> commonTable)
            : base(commonTable)
        { }

        /// <summary>
        /// Get instance of the "RatingScaleService" class
        /// </summary>
        /// <returns>"RatingScaleService" class unique object</returns>
        public static RatingScaleService GetInstance()
        {
            if (_uniqueInstance == null)
                lock (_tableName)
                    if (_uniqueInstance == null)
                        _uniqueInstance = new RatingScaleService(CommonTableFactory.CreateCommonTable<RatingScaleDataModel>(_tableName));
            return _uniqueInstance;
        }

        /// <summary>
        /// Get records of the table by field "Profiles_ID"
        /// </summary>
        /// <param name="profilesID">"Profiles_ID" field value</param>
        /// <returns>Records that match the search condition</returns>
        public IQueryable<RatingScaleDataModel> GetRecordsByProfilesID(int profilesID)
        {
            return
                from r in GetRecords()
                where r.ProfilesID == profilesID
                select r;
        }

        /// <summary>
        /// Get max ID
        /// </summary>
        public int GetMaxID()
        {
            int iddata = 0;
            try
            {
                iddata = (from aitem in GetRecords()
                          orderby aitem.ID descending
                          select aitem.ID).FirstOrDefault<int>();
                iddata += 1;
            }
            catch (Exception ex)
            {
                Log.ErrorLog(ex, "Невозможно определить новый id записи");
            }
            return iddata;
        }

        /// <summary>
        /// Add RatingScale data
        /// </summary>
        /// <param name="record">New object of RatingScale</param>
        public void AddData(RatingScaleDataModel record)
        {
            record.ID = GetMaxID();
            base.InsertRecord(record);
        }

        /// <summary>
        /// Update RatingScale data
        /// </summary>
        /// <param name="idoldobj">ID value of RatingScale old object</param>
        /// <param name="newobj">Updated object of RatingScale</param>
        public void EditData(int idoldobj, RatingScaleDataModel newobj)
        {
            var edit = GetRecords().Where(o => o.ID == idoldobj).FirstOrDefault();

            if (edit != null && newobj != null)
            {
                edit.ID = newobj.ID;
                edit.ProfilesID = newobj.ProfilesID;
                edit.Text = newobj.Text;
                edit.LowBound = newobj.LowBound;
            }
        }

        /// <summary>
        /// Remove RatingScale data
        /// </summary>
        /// <param name="obj">Removing object of RatingScale</param>
        public void RemoveData(int id)
        {
            var record = GetRecords().Where(o => o.ID == id).FirstOrDefault();
            if (record != null)
                DeleteRecord(record);
        }
    }
}
