﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using CommonClassesLib.Interfaces;
using CommonClassesLib.Tools;

namespace CommonClassesLib.Services
{
    sealed class SerializeCommonTable<T>: ICommonTable<T>
    {
        private List<T> _table;
        private string _tableName;
        
        public SerializeCommonTable(string tableName)
        {
            if (string.IsNullOrEmpty(tableName))
                throw new ArgumentException();

            _tableName = tableName;
            _table = new List<T>();

            try
            {
                if (File.Exists(tableName))
                    using (FileStream fs = new FileStream(tableName, FileMode.Open, FileAccess.Read))
                    {
                        XmlSerializer xs = new XmlSerializer(typeof(List<T>));
                        _table = (List<T>)xs.Deserialize(fs);
                    }
            }
            catch (Exception ex)
            {
                Log.ErrorLog(ex);
            }
        }

        public void DeleteData(T item)
        {
            if (item != null)
                _table.Remove(item);
        }

        public IQueryable<T> GetData()
        {
            return _table.AsQueryable<T>();
        }

        public void InsertData(T item)
        {
            if (item != null && !ExistsData(item))
                _table.Add(item);
        }

        public bool ExistsData(T item)
        {
            if (item != null)
                return _table.Contains(item);
            else
                return false;
        }

        public void SaveData()
        {
            try
            {
                Directory.CreateDirectory(Path.GetDirectoryName(_tableName));

                using (FileStream fs = new FileStream(_tableName, FileMode.Create, FileAccess.Write))
                {
                    using (StreamWriter sw = new StreamWriter(fs, Encoding.UTF8))
                    {
                        XmlSerializerNamespaces ns = new XmlSerializerNamespaces();
                        ns.Add("", "");
                        XmlSerializer xs = new XmlSerializer(typeof(List<T>));
                        xs.Serialize(sw, _table, ns);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.ErrorLog(ex);
            }
        }
    }
}
