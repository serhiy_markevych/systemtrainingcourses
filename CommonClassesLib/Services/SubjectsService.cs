﻿using System.Collections.Generic;
using System.Linq;
using CommonClassesLib.DataModels;
using CommonClassesLib.Interfaces;
using System;
using CommonClassesLib.Tools;

namespace CommonClassesLib.Services
{
    /// <summary>
    /// "Subjects" table services
    /// </summary>
    public sealed class SubjectsService: CommonService<SubjectDataModel>
    {
        private volatile static SubjectsService _uniqueInstance = null;
        private static string _tableName = "Subjects";

        private SubjectsService(ICommonTable<SubjectDataModel> commonTable)
            : base(commonTable)
        { }

        /// <summary>
        /// Get instance of the "SubjectsService" class
        /// </summary>
        /// <returns>"SubjectsService" class unique object</returns>
        public static SubjectsService GetInstance()
        {
            if (_uniqueInstance == null)
                lock (_tableName)
                    if (_uniqueInstance == null)
                        _uniqueInstance = new SubjectsService(CommonTableFactory.CreateCommonTable<SubjectDataModel>(_tableName));
            return _uniqueInstance;
        }

        /// <summary>
        /// Get records of the table by field "Сanceled_mark"
        /// </summary>
        /// <param name="canceledMark">"Canceled_mark" field value</param>
        /// <returns>Records that match the search condition</returns>
        public IQueryable<SubjectDataModel> GetRecordsByCanceledMark(int canceledMark = 0)
        {
            return
                from i in GetRecords()
                where i.CanceledMark == canceledMark
                select i;
        }

        /// <summary>
        /// Get record of the table by field "ID"
        /// </summary>
        /// <param name="id">"ID" field value</param>
        /// <returns>Record that matches the search condition</returns>
        public SubjectDataModel GetRecordByID(int id)
        {
            return
                (from i in GetRecords()
                 where i.ID == id
                 select i).FirstOrDefault();
        }

        /// <summary>
        /// Get max ID
        /// </summary>
        public int GetMaxID()
        {
            int iddata = 0;
            try
            {
                iddata = (from aitem in GetRecords()
                          orderby aitem.ID descending
                          select aitem.ID).FirstOrDefault<int>();
                iddata += 1;
            }
            catch (Exception ex)
            {
                Log.ErrorLog(ex, "Невозможно определить новый id записи");
            }
            return iddata;
        }

        /// <summary>
        /// Add Subjects data
        /// </summary>
        /// <param name="record">New object of Subjects</param>
        public void AddData(SubjectDataModel record)
        {
            record.ID = GetMaxID();
            base.InsertRecord(record);
        }

        /// <summary>
        /// Update Subjects data
        /// </summary>
        /// <param name="idoldobj">ID value of Subjects old object</param>
        /// <param name="newobj">Updated object of Subjects</param>
        public void EditData(int idoldobj, SubjectDataModel newobj)
        {
            var edit = GetRecords().Where(o => o.ID == idoldobj).FirstOrDefault();

            if (edit != null && newobj != null)
            {
                edit.ID = newobj.ID;
                edit.Name = newobj.Name;
                edit.CanceledMark = newobj.CanceledMark;
            }
        }

        /// <summary>
        /// Remove Subjects data
        /// </summary>
        /// <param name="obj">Removing object of Subjects</param>
        public void RemoveData(int id)
        {
            var record = GetRecords().Where(o => o.ID == id).FirstOrDefault();
            if (record != null)
                DeleteRecord(record);
        }
    }
}
