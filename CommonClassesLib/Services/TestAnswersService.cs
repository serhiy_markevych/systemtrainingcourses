﻿using System.Collections.Generic;
using System.Linq;
using CommonClassesLib.DataModels;
using CommonClassesLib.Interfaces;

namespace CommonClassesLib.Services
{
    /// <summary>
    /// "Test_answers" table services
    /// </summary>
    public sealed class TestAnswersService: CommonService<TestAnswerDataModel>
    {
        private volatile static TestAnswersService _uniqueInstance = null;
        private static string _tableName = "Test_answers";

        private TestAnswersService(ICommonTable<TestAnswerDataModel> commonTable)
            : base(commonTable)
        { }

        /// <summary>
        /// Get instance of the "TestAnswersService" class
        /// </summary>
        /// <returns>"TestAnswersService" class unique object</returns>
        public static TestAnswersService GetInstance()
        {
            if (_uniqueInstance == null)
                lock (_tableName)
                    if (_uniqueInstance == null)
                        _uniqueInstance = new TestAnswersService(CommonTableFactory.CreateCommonTable<TestAnswerDataModel>(_tableName));
            return _uniqueInstance;
        }

        /// <summary>
        /// Get records of the table by field "Test_questions_ID"
        /// </summary>
        /// <param name="testQuestionsID">"Test_questions_ID" field value</param>
        /// <returns>Records that match the search condition</returns>
        public IQueryable<TestAnswerDataModel> GetRecordsByTestQuestionsID(int testQuestionsID)
        {
            return
                from t in GetRecords()
                where t.TestQuestionsID == testQuestionsID
                select t;
        }

        /// <summary>
        /// Get the maximum value of the field "ID", plus one
        /// </summary>
        /// <returns>Incremented maximum value of the field "ID"</returns>
        private int GetMaxID()
        {
            if (GetRecords().Count() > 0)
                return
                    (from t in GetRecords()
                     select t.ID).Max() + 1;
            else
                return 1;
        }

        /// <summary>
        /// Adds record to a table
        /// </summary>
        /// <param name="record">Record that must be inserted</param>
        public override void InsertRecord(TestAnswerDataModel record)
        {
            record.ID = GetMaxID();
            base.InsertRecord(record);
        }
    }
}
