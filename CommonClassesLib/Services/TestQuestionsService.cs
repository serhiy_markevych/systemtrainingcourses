﻿using System.Linq;
using CommonClassesLib.DataModels;
using CommonClassesLib.Interfaces;

namespace CommonClassesLib.Services
{
    /// <summary>
    /// "Test_questions" table services
    /// </summary>
    public sealed class TestQuestionsService: CommonService<TestQuestionDataModel>
    {
        private volatile static TestQuestionsService _uniqueInstance = null;
        private static string _tableName = "Test_questions";

        private TestQuestionsService(ICommonTable<TestQuestionDataModel> commonTable)
            : base(commonTable)
        { }

        /// <summary>
        /// Get instance of the "TestQuestionsService" class
        /// </summary>
        /// <returns>"TestQuestionsService" class unique object</returns>
        public static TestQuestionsService GetInstance()
        {
            if (_uniqueInstance == null)
                lock (_tableName)
                    if (_uniqueInstance == null)
                        _uniqueInstance = new TestQuestionsService(CommonTableFactory.CreateCommonTable<TestQuestionDataModel>(_tableName));
            return _uniqueInstance;
        }

        /// <summary>
        /// Get the maximum value of the field "ID", plus one
        /// </summary>
        /// <returns>Incremented maximum value of the field "ID"</returns>
        private int GetMaxID()
        {
            if (GetRecords().Count() > 0)
                return
                    (from tq in GetRecords()
                     select tq.ID).Max() + 1;
            else
                return 1;
        }

        /// <summary>
        /// Adds record to a table
        /// </summary>
        /// <param name="record">Record that must be inserted</param>
        public override void InsertRecord(TestQuestionDataModel record)
        {
            record.ID = GetMaxID();
            base.InsertRecord(record);
        }
    }
}
