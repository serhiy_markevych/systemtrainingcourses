﻿using System.Linq;
using CommonClassesLib.DataModels;
using CommonClassesLib.Interfaces;

namespace CommonClassesLib.Services
{
    /// <summary>
    /// "Tests" table services
    /// </summary>
    public sealed class TestsService : CommonService<TestDataModel>
    {
        private volatile static TestsService _uniqueInstance = null;
        private static string _tableName = "Tests";

        private TestsService(ICommonTable<TestDataModel> commonTable)
            : base(commonTable)
        { }

        /// <summary>
        /// Get instance of the "TestsService" class
        /// </summary>
        /// <returns>"TestsService" class unique object</returns>
        public static TestsService GetInstance()
        {
            if (_uniqueInstance == null)
                lock (_tableName)
                    if (_uniqueInstance == null)
                        _uniqueInstance = new TestsService(CommonTableFactory.CreateCommonTable<TestDataModel>(_tableName));
            return _uniqueInstance;
        }

        /// <summary>
        /// Get the maximum value of the field "ID", plus one
        /// </summary>
        /// <returns>Incremented maximum value of the field "ID"</returns>
        private int GetMaxID()
        {
            if (GetRecords().Count() > 0)
                return
                    (from t in GetRecords()
                     select t.ID).Max() + 1;
            else
                return 1;
        }

        /// <summary>
        /// Adds record to a table
        /// </summary>
        /// <param name="record">Record that must be inserted</param>
        public override void InsertRecord(TestDataModel record)
        {
            record.ID = GetMaxID();
            base.InsertRecord(record);
        }

        /// <summary>
        /// Get record of the table by field "ID"
        /// </summary>
        /// <param name="id">"ID" field value</param>
        /// <returns>Record that matches the search condition</returns>
        public TestDataModel GetRecordByID(int id)
        {
            return
                (from t in GetRecords()
                 where t.ID == id
                 select t).FirstOrDefault();
        }

        /// <summary>
        /// Get records of the table by field "Student_ID"
        /// </summary>
        /// <param name="studentID">"Student_ID" field value</param>
        /// <returns>Records that match the search condition</returns>
        public IQueryable<TestDataModel> GetRecordsByStudentID(int studentID)
        {
            return
                from t in GetRecords()
                where t.StudentID == studentID
                select t;
        }

        /// <summary>
        /// Get records of the table by field "Teacher_ID"
        /// </summary>
        /// <param name="teacherID">"Teacher_ID" field value</param>
        /// <returns>Records that match the search condition</returns>
        public IQueryable<TestDataModel> GetRecordsByTeacherID(int teacherID)
        {
            return
                from t in GetRecords()
                where t.TeacherID == teacherID
                select t;
        }

        /// <summary>
        /// Get records of the table by field "Subjects_ID"
        /// </summary>
        /// <param name="subjectsID">"Subjects_ID" field value</param>
        /// <returns>Records that match the search condition</returns>
        public IQueryable<TestDataModel> GetRecordsBySubjectID(int subjectsID)
        {
            return
                from t in GetRecords()
                where t.SubjectsID == subjectsID
                select t;
        }

        /// <summary>
        /// Get records of the table by student group ID
        /// </summary>
        /// <param name="groupsID">Student group ID</param>
        /// <returns>Records that match the search condition</returns>
        public IQueryable<TestDataModel> GetRecordsByGroupsID(int groupsID)
        {
            UserService uService = UserService.GetInstance();

            return from u in uService.GetUsersByGroup(groupsID).AsQueryable()
                   join t in GetRecords() on u.Id equals t.StudentID
                   select t;
        }
    }
}
