﻿using System;
using System.Collections.Generic;
using System.Linq;
using CommonClassesLib.Exceptions;
using CommonClassesLib.DataModels;
using CommonClassesLib.Interfaces;
using CommonResourcesLib;

namespace CommonClassesLib.Services
{
    public class UserService : CommonService<UserDataModel>
    {
        #region Fields
        private volatile static UserService _uniqueInstance = null;
        private static string _tableName = "Users";
        #endregion


        #region Constructors
        private UserService(ICommonTable<UserDataModel> commonTable)
            : base(commonTable)
        { }
        #endregion


        #region Methods
        /// <summary>
        /// Get a reference to the class object AnswersService
        /// </summary>
        /// <returns>Unique object class AnswersService</returns>
        public static UserService GetInstance()
        {
            if (_uniqueInstance == null)
                lock (_tableName)
                    if (_uniqueInstance == null)
                        _uniqueInstance = new UserService(CommonTableFactory.CreateCommonTable<UserDataModel>(_tableName));
            return _uniqueInstance;
        }

        /// <summary>
        /// Get collection of users from database
        /// </summary>        
        /// <returns>List of UserDataModel</returns>
        public List<UserDataModel> GetUsers()
        {
            GroupService gService = GroupService.GetInstance();
            List<GroupDataModel> allGroups = gService.GetGroups();
            List<UserDataModel> allUsers = base.GetRecords().ToList<UserDataModel>();

            // getting group from groups database by foreign key
            List<UserDataModel> users = (from u in allUsers
                                         join g in allGroups on u.GroupId equals g.Id into gs
                                         from g in gs.DefaultIfEmpty()
                                         select new UserDataModel()
                                         {
                                             FirstName = u.FirstName,
                                             Group = g,
                                             Email = u.Email,
                                             GroupId = u.GroupId,
                                             HashedPassword = u.HashedPassword,
                                             LastName = u.LastName,
                                             MiddleName = u.MiddleName,
                                             Username = u.Username,
                                             Roles = u.Roles,
                                             Id = u.Id
                                         }).ToList<UserDataModel>();

            return users;
        }

        /// <summary>
        /// Method will return user with specified id
        /// </summary>
        /// <param name="id">user id</param>        
        /// <returns>UserDataModel user</returns>
        public UserDataModel GetUser(int id)
        {
            var users = from u in GetRecords()
                        where u.Id == id
                        select u;

            UserDataModel user = users.FirstOrDefault();

            if (user != null)
            {
                GroupService gService = GroupService.GetInstance();
                user.Group = gService.GetGroup(user.GroupId);
            }

            return user;
        }

        /// <summary>
        /// Method will return user by specified username
        /// </summary>
        /// <param name="username">username</param>
        /// <returns>UserDataModel user</returns>
        public UserDataModel GetUser(string username)
        {
            var users = from u in GetRecords()
                        where u.Username == username
                        select u;

            UserDataModel user = users.FirstOrDefault();

            if (user != null)
            {
                GroupService gService = GroupService.GetInstance();
                user.Group = gService.GetGroup(user.GroupId);
            }
            return user;
        }

        /// <summary>
        /// Method will return user by group
        /// </summary>
        /// <param name="groupId">id student group</param>
        /// <returns>List students type of UserDataModel</returns>
        public List<UserDataModel> GetUsersByGroup(int groupId)
        {

            List<UserDataModel> usersByGroup = new List<UserDataModel>();

            var users = from u in this.GetUsers()
                        where u.GroupId == groupId
                        select u;

            if (users != null)
            {
                foreach (UserDataModel item in users)
                    usersByGroup.Add(item);
            }

            return usersByGroup;
        }

        /// <summary>
        /// Add new user to database
        /// </summary>
        /// <param name="user">user object to add</param>
        /// <returns>void</returns>
        public void AddUser(UserDataModel user)
        {
            if (this.IsUnique(this.GetUsers(), user))
            {
                user.Id = this.GetNewId();
                base.InsertRecord(user);
                base.SubminChanges();
            }
            else
            {
                throw new TrainingException(Errors.UserAlreadyExist);
            }

        }

        /// <summary>
        /// Get new ID for user
        /// </summary>
        /// <returns></returns>
        public int GetNewId()
        {
            if (GetRecords().Count() > 0)
                return
                    (from t in GetRecords()
                     select t.Id).Max() + 1;
            else
                return 0;
        }

        /// <summary>
        /// Check uniqueness of specified user in collection
        /// </summary>
        /// <param name="users">collection of users</param>
        /// <param name="userToCheck">user to verify uniqueness</param>
        /// <returns>bool</returns>
        public bool IsUnique(List<UserDataModel> users, UserDataModel userToCheck)
        {
            // try select from collection user with same data that in userToCheck
            var result = from u in users
                         where u.Username == userToCheck.Username
                             || u.Email == userToCheck.Email
                         select u;
            UserDataModel user = result.FirstOrDefault();

            // if user exist than userToCheck in not unique
            if (user != null) return false;
            else return true;
        }


        /// <summary>
        /// Delete user by username from database
        /// </summary>
        /// <param name="username">username</param>
        /// <returns>void</returns>
        public void DeleteUser(string username)
        {
            UserDataModel userToDelete = this.GetUser(username);

            if (userToDelete != null)
            {
                this.DeleteRecord(userToDelete);
                this.SubminChanges();
            }
        }
        #endregion

    }
}