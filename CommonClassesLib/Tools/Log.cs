﻿using System;
using CommonResourcesLib;
using System.Text;
using System.IO;

namespace CommonClassesLib.Tools
{
    sealed public class Log
    {
        /// <summary>
        /// Adds error message into the log
        /// </summary>
        /// <param name="exception">Exception</param>
        /// <param name="message">Message text</param>
        /// <param name="details">Exeption details</param>
        public static void ErrorLog(Exception exception, string message = "", string details = "")
        {
            StringBuilder logString = new StringBuilder();
            logString.AppendFormat("{0} ", DateTime.Now.ToUniversalTime());

            if (string.IsNullOrEmpty(message))
                logString.AppendLine(exception != null ? exception.Message : Errors.Error);
            else
                logString.AppendLine(message);

            if (string.IsNullOrEmpty(details))
            {
                if (exception != null)
                    logString.AppendLine(exception.StackTrace);
            }
            else
                logString.AppendLine(details);

            SaveLogToFile(logString.ToString());
            
        }

        private static void SaveLogToFile(string message, string path = @"..\..\..\Logs\Error.log")
        {
            Directory.CreateDirectory(Path.GetDirectoryName(path));
            using (StreamWriter writer = File.AppendText(path))
            {
                writer.WriteLine(message);
            }
        }
    }
}
