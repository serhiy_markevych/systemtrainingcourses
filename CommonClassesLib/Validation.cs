﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using CommonClassesLib.Exceptions;
using CommonResourcesLib;

namespace CommonClassesLib
{
    public static class Validation
    {
        /// <summary>
        /// Validation of email addresses
        /// </summary>
        /// <param name="email"></param>
        /// <returns>Bool</returns>
        public static bool ValidEmail(string email)
        {
            try
            {
                bool valid = false;

                if (string.IsNullOrEmpty(email))
                {
                    valid = false;
                }
                else
                {
                    Regex rgxEmail = new Regex(RegExpressions.mail, RegexOptions.IgnoreCase);

                    valid = rgxEmail.IsMatch(email);
                }

                return valid;
            }
            catch (Exception)
            {
                throw;
            }

        }

        /// <summary>
        /// Validation of username
        /// </summary>
        /// <param name="username"></param>
        /// <returns>Bool</returns>
        public static bool ValidUsername(string username)
        {
            try
            {
                bool valid = false;

                byte minLenght = 3, maxLength = 25;

                if (username.Length >= minLenght && username.Length <= maxLength)
                {
                    Regex rgxUsername = new Regex(RegExpressions.username);

                    valid = rgxUsername.IsMatch(username);
                }

                return valid;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Validation of password
        /// </summary>
        /// <param name="password"></param>
        /// <returns>Bool</returns>
        public static bool ValidPassword(string password)
        {
            try
            {
                bool valid = false;

                if (password.Length >= 5)
                    valid = true;

                return valid;

            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
