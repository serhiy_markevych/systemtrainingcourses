using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CommonResourcesLib;

namespace StatisticLib.DataModels
{
    public class StudentStatisticDataModel
    {
        #region Fields
        private string _subjectName;
        private int _totalQuestions;
        private int _totalAnswers;
        private int _studentRating;
        private int _totalRating;
        //private int _lowBound;        
        private DateTime _deliveryDate;
        private string _teacherFullName;
        private int _testTime;
        private TimeSpan _elapsedTime;
        #endregion

        #region Constructors
        public StudentStatisticDataModel()
        {
            _deliveryDate = new DateTime();
            _subjectName = "";
            _teacherFullName = "";
            _testTime = -1;
            _elapsedTime = new TimeSpan();
            _totalQuestions = -1;
            _totalAnswers = -1;
            _studentRating = -1;
            _totalRating = -1;
        }
        public StudentStatisticDataModel(DateTime passTestDate, string subjectName, string teacherFullName, int testTime, TimeSpan elapsedTime, int questionsCount, int correctAnswers)
        {
            _deliveryDate = passTestDate;
            _subjectName = subjectName;
            _teacherFullName = teacherFullName;
            _testTime = testTime;
            _elapsedTime = elapsedTime;
            _totalQuestions = questionsCount;
            _totalAnswers = correctAnswers;
        }
        #endregion

        #region Methods
        public override string ToString()
        {
            string isPassed = "";
            if (IsPassed) isPassed = Messages.TestPassed;
            else isPassed = Messages.TestFailed;

            return String.Format(Messages.StudentStatisticToString,
                SubjectName, TeacherFullName, DeliveryDate, TotalAnswers, TotalQuestions, PercentageAnswers, StudentRating, TotalRating, isPassed, ElapsedTime, TestTime);
        }
        #endregion

        #region Properties
        /// <summary>
        /// Subject name of test
        /// </summary>
        public string SubjectName
        {
            get { return _subjectName; }
            set { _subjectName = value; }
        }
        /// <summary>
        /// Maximum number of questions in test
        /// </summary>
        public int TotalQuestions
        {
            get { return _totalQuestions; }
            set { _totalQuestions = value; }
        }
        /// <summary>
        /// Number of correct answers
        /// </summary>
        public int TotalAnswers
        {
            get { return _totalAnswers; }
            set { _totalAnswers = value; }
        }
        /// <summary>
        /// Number of points scored by student
        /// </summary>
        public int StudentRating
        {
            get { return _studentRating; }
            set { _studentRating = value; }
        }
        /// <summary>
        /// Maximum number of points
        /// </summary>
        public int TotalRating
        {
            get { return _totalRating; }
            set { _totalRating = value; }
        }
        /// <summary>
        /// Minimum bound for passing the test
        /// </summary>
        /*public int LowBound
        {
            get { return _lowBound; }
            set { _lowBound = value; }
        }*/
        /// <summary>
        /// Is the test was passed
        /// </summary>
        public bool IsPassed
        {
            get { return (StudentRating > TotalRating / 2) ? true : false; }
        }
        /// <summary>
        /// Date of passing the test
        /// </summary>
        public DateTime DeliveryDate
        {
            get { return _deliveryDate; }
            set { _deliveryDate = value; }
        }        
        /// <summary>
        /// FullName of teacher who created the test
        /// </summary>
        public string TeacherFullName
        {
            get { return _teacherFullName; }
            set { _teacherFullName = value; }
        }
        /// <summary>
        /// Time limit for passing test
        /// </summary>
        public int TestTime
        {
            get { return _testTime; }
            set { _testTime = value; }
        }
        /// <summary>
        /// Time taken for test
        /// </summary>
        public TimeSpan ElapsedTime
        {
            get { return _elapsedTime; }
            set { _elapsedTime = value; }
        }
        /// <summary>
        /// Percentage of correct answers
        /// </summary>
        public double PercentageAnswers
        {
            get { return _totalAnswers * 100 / _totalQuestions; }
        }
        #endregion
    }
}
