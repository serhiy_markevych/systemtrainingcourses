using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CommonClassesLib.DataModels;
using CommonClassesLib.Services;
using CommonResourcesLib;

namespace StatisticLib.DataModels
{
    public class TeacherStatisticDataModel
    {
        #region Fields
        private string _subjectName;
        private UserDataModel _student;
        private string _groupName;
        private DateTime _deliveryDate;
        private int _testTime;
        private TimeSpan _elapsedTime;
        private int _totalQuestions;
        private int _totalAnswers;
        private int _totalRating;
        private int _studentRating;
        #endregion

        #region Property
        /// <summary>
        /// Subject name of test
        /// </summary>
        public string SubjectName
        {
            get { return _subjectName; }
            set { _subjectName = value; }
        }
        /// <summary>
        /// Student who has passed the test
        /// </summary>
        public UserDataModel Student
        {
            get { return _student; }
            set { _student = value; }
        }
        /// <summary>
        /// Student study group
        /// </summary>
        public string GroupName
        {
            get { return _groupName; }
            set { _groupName = value; }
        }
        /// <summary>
        /// Date of passing the test
        /// </summary>
        public DateTime DeliveryDate
        {
            get { return _deliveryDate; }
            set { _deliveryDate = value; }
        }
        /// <summary>
        /// Time limit for passing test
        /// </summary>
        public int TestTime
        {
            get { return _testTime; }
            set { _testTime = value; }
        }
        /// <summary>
        /// Time taken for test
        /// </summary>
        public TimeSpan ElapsedTime
        {
            get { return _elapsedTime; }
            set { _elapsedTime = value; }
        }
        /// <summary>
        /// Maximum number of questions in test
        /// </summary>
        public int TotalQuestions
        {
            get { return _totalQuestions; }
            set { _totalQuestions = value; }
        }
        /// <summary>
        /// Number of correct answers
        /// </summary>
        public int TotalAnswers
        {
            get { return _totalAnswers; }
            set { _totalAnswers = value; }
        }
        /// <summary>
        /// Number of points scored by student
        /// </summary>
        public int StudentRating
        {
            get { return _studentRating; }
            set { _studentRating = value; }
        }
        /// <summary>
        /// Maximum number of points
        /// </summary>
        public int TotalRating
        {
            get { return _totalRating; }
            set { _totalRating = value; }
        }
        /// <summary>
        /// Is the test was passed
        /// </summary>
        public bool IsPassed
        {
            get { return (StudentRating > TotalRating / 2) ? true : false; }
        }
        /// <summary>
        /// Percentage of correct answers
        /// </summary>
        public double PercentageAnswers
        {
            get { return _totalAnswers * 100 / _totalQuestions; }
        }
        #endregion

        #region Constructors
        public TeacherStatisticDataModel()
        {

        }
        #endregion

        #region Methods
        public override string ToString()
        {
            string isPassed = "";

            if (IsPassed)
            {
                isPassed = Messages.TestPassed;
            }
            else 
            {
                isPassed = Messages.TestFailed;
            }

            return String.Format(Messages.TeacherStatisticToString,
                DeliveryDate, SubjectName, GroupName,Student.LastName,Student.FirstName,TotalAnswers,TotalQuestions,PercentageAnswers,isPassed);
        }
        #endregion
    }
}
