using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using StatisticLib.DataModels;
using CommonClassesLib.DataModels;
using CommonClassesLib.Services;
using System.IO;

namespace StatisticLib.Services
{
    public class StatisticService
    {
        #region Methods
        /// <summary>
        /// Get Student Statistic by Test
        /// </summary>
        /// <param name="studentId">Student ID</param>
        /// <param name="testId">Test ID</param>
        /// <returns>StudentStatisticDataModel Model</returns>
        public StudentStatisticDataModel GetStudentStatistic(int studentId, int testId)
        {
            List<StudentStatisticDataModel> statistics = new List<StudentStatisticDataModel>();
            TestsService testService = TestsService.GetInstance();

            var selectTests = from test in testService.GetRecordsByStudentID(studentId)
                              where test.ID == testId
                              select test;

            statistics = FormationStudentStatisticList(selectTests.ToList<TestDataModel>());

            return statistics.FirstOrDefault();
        }


        /// <summary>
        /// Get Student Statistics by Subject
        /// </summary>
        /// <param name="studentId">Student ID</param>
        /// <param name="subjectId">Subject ID</param>
        /// <returns>List StudentStatisticDataModel Models</returns>
        public List<StudentStatisticDataModel> GetStudentStatisticBySubject(int studentId, int subjectId)
        {
            List<StudentStatisticDataModel> statistics = new List<StudentStatisticDataModel>();
            TestsService testService = TestsService.GetInstance();

            var selectTests = from test in testService.GetRecordsByStudentID(studentId)
                              where test.SubjectsID == subjectId
                              select test;

            statistics = FormationStudentStatisticList(selectTests.ToList<TestDataModel>());

            return statistics.Count > 0 ? statistics : null;
        }


        /// <summary>
        /// Get All Student Statistics
        /// </summary>
        /// <param name="studentId">Student ID</param>        
        /// <returns>List StudentStatisticDataModel Models</returns>
        public List<StudentStatisticDataModel> GetAllStudentStatistics(int studentId)
        {
            List<StudentStatisticDataModel> statistics = new List<StudentStatisticDataModel>();
            TestsService testService = TestsService.GetInstance();
            var tests = testService.GetRecordsByStudentID(studentId).ToList<TestDataModel>();
            statistics = FormationStudentStatisticList(tests);

            return statistics.Count > 0 ? statistics : null;
        }


        /// <summary>
        /// Get Teacher Statistic By Teacher And Subject
        /// </summary>
        /// <param name="teacherId">Teacher ID</param>
        /// <param name="subjectId">Subject ID</param>
        /// <returns>List TeacherStatisticDataModel Models</returns>
        public List<TeacherStatisticDataModel> GetTeacherStatisticByTeacherAndSubject(int teacherId, int subjectId)
        {
            List<TeacherStatisticDataModel> statisticList = new List<TeacherStatisticDataModel>();

            TestsService testService = TestsService.GetInstance();

            List<TestDataModel> selectTests = (from test in testService.GetRecordsBySubjectID(subjectId)
                                               where test.TeacherID == teacherId
                                               select test).ToList<TestDataModel>();

            statisticList = FormationTeacherStatisticList(selectTests);

            return statisticList;
        }


        /// <summary>
        /// Get Teacher Statistic By Student And Subject
        /// </summary>
        /// <param name="studentId">Student ID</param>
        /// <param name="subjectId"> Subject ID</param>
        /// <returns>List TeacherStatisticDataModel Models</returns>
        public List<TeacherStatisticDataModel> GetTeacherStatisticByStudentAndSubject(int studentId, int subjectId)
        {
            List<TeacherStatisticDataModel> statisticList = new List<TeacherStatisticDataModel>();

            TestsService testService = TestsService.GetInstance();

            List<TestDataModel> selectTests = (from test in testService.GetRecordsByStudentID(subjectId)
                                               where test.SubjectsID == subjectId
                                               select test).ToList<TestDataModel>();

            statisticList = FormationTeacherStatisticList(selectTests);

            return statisticList;
        }


        /// <summary>
        /// Get Teacher Statistic By Group And Subject
        /// </summary>
        /// <param name="groupId">Group ID</param>
        /// <param name="subjectId">Subject ID</param>
        /// <returns>List TeacherStatisticDataModel Models</returns>
        public List<TeacherStatisticDataModel> GetTeacherStatisticByGroupAndSubject(int groupId, int subjectId)
        {
            List<TeacherStatisticDataModel> statisticList = new List<TeacherStatisticDataModel>();

            TestsService testService = TestsService.GetInstance();

            var buff = testService.GetRecordsByGroupsID(groupId);

            List<TestDataModel> selectTests = (from test in testService.GetRecordsByGroupsID(groupId)
                                               where test.SubjectsID == subjectId
                                               select test).ToList<TestDataModel>();

            statisticList = FormationTeacherStatisticList(selectTests);

            return statisticList;
        }


        /// <summary>
        /// Method forming TeacherStatisticDataModel model
        /// </summary>
        /// <param name="selectTests">List of tests selection</param>
        /// <returns>List TeacherStatisticDataModel Models</returns>
        protected List<TeacherStatisticDataModel> FormationTeacherStatisticList(List<TestDataModel> selectTests)
        {
            List<TeacherStatisticDataModel> resultList = new List<TeacherStatisticDataModel>();

            if (selectTests != null)
            {
                ProfilesService profileService = ProfilesService.GetInstance();
                SubjectsService subjectService = SubjectsService.GetInstance();

                UserService userService = UserService.GetInstance();

                foreach (var itemTest in selectTests)
                {
                    TeacherStatisticDataModel tempTeacherModel = new TeacherStatisticDataModel();

                    tempTeacherModel.SubjectName = subjectService.GetRecordByID(itemTest.SubjectsID).Name;
                    tempTeacherModel.Student = userService.GetUser(itemTest.StudentID);
                    tempTeacherModel.GroupName = tempTeacherModel.Student.Group.Name;
                    tempTeacherModel.DeliveryDate = itemTest.Date;
                    tempTeacherModel.TestTime = profileService.GetRecordByID(itemTest.ProfilesID).TimeLimit;
                    tempTeacherModel.ElapsedTime = itemTest.Duration;
                    tempTeacherModel.TotalQuestions = itemTest.TotalQuestions;
                    tempTeacherModel.TotalAnswers = itemTest.TotalAnswers;
                    tempTeacherModel.TotalRating = itemTest.TotalRating;
                    tempTeacherModel.StudentRating = itemTest.Rating;

                    resultList.Add(tempTeacherModel);
                }
            }

            return resultList;
        }


        /// <summary>
        /// Method forming StudentStatisticDataModel model
        /// </summary>
        /// <param name="selectTests">List of tests</param>
        /// <returns>List TeacherStatisticDataModel Models</returns>
        protected List<StudentStatisticDataModel> FormationStudentStatisticList(List<TestDataModel> tests)
        {
            List<StudentStatisticDataModel> result = new List<StudentStatisticDataModel>();

            if (tests != null)
            {
                ProfilesService profileService = ProfilesService.GetInstance();
                SubjectsService subjectService = SubjectsService.GetInstance();
                UserService userService = UserService.GetInstance();

                foreach (var test in tests)
                {
                    StudentStatisticDataModel newStudentStatistic = new StudentStatisticDataModel();

                    newStudentStatistic.SubjectName = subjectService.GetRecordByID(test.SubjectsID).Name;
                    newStudentStatistic.TotalQuestions = test.TotalQuestions;
                    newStudentStatistic.TotalAnswers = test.TotalAnswers;
                    newStudentStatistic.StudentRating = test.Rating;
                    newStudentStatistic.TotalRating = test.TotalRating;
                    //newStudentStatistic.LowBound = 0;
                    newStudentStatistic.DeliveryDate = test.Date;
                    UserDataModel teacher = userService.GetUser(test.TeacherID);
                    newStudentStatistic.TeacherFullName = teacher.LastName + " " + teacher.FirstName + " " + teacher.MiddleName;
                    newStudentStatistic.TestTime = profileService.GetRecordByID(test.ProfilesID).TimeLimit;
                    newStudentStatistic.ElapsedTime = test.Duration;

                    result.Add(newStudentStatistic);
                }
            }

            return result;
        }
        #endregion
    }
}