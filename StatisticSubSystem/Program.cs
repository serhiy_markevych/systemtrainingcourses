using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using CommonClassesLib.Services;
using CommonClassesLib.DataModels;
using StatisticLib.Services;
using StatisticLib.DataModels;
using System.Threading;
using CommonClassesLib;
using AuthenticationLib;


namespace StatisticSubSystem
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Title = "Statistica v. 1.1";

            do
            {
                try
                {
                    Console.Write("Please enter your username: ");
                    string username = Console.ReadLine();
                    UserService uService = UserService.GetInstance();
                    // if that user is registered (is exist in db)
                    if (uService.GetUser(username) == null)
                    {
                        Console.WriteLine("That user does not register in system.");
                        continue;
                    }
                    Console.Write("Enter your password: ");
                    string password = Console.ReadLine();

                    UserDataModel currentUser = Authentication.Login(username, password);

                    if (currentUser != null) //if auth successfull
                    {
                        // if authorized user is student
                        if (currentUser.HasRole(Role.Student))
                        {
                            StudentCapabilities(currentUser);
                        }
                        // if authorized user is teacher
                        else if (currentUser.HasRole(Role.Teacher))
                        {
                            TeacherCapabilities(currentUser);
                        }
                    }
                    else // if auth failed
                    {
                        Console.Write("Username or password is not correct.");

                    }

                    Console.Write("Run application again? [Y/any]\n");
                    ConsoleKeyInfo continueKey = Console.ReadKey(true);
                    if (continueKey.KeyChar == 'Y' || continueKey.KeyChar == 'y' || continueKey.Key == ConsoleKey.Enter)
                    {
                        Console.Clear();
                        continue;
                    }
                    else break;
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            } while (true);

            Console.WriteLine("\nPress any key to close programm.");
            Console.ReadKey();
        }

        public static void StudentCapabilities(UserDataModel student)
        {
            bool isActive = true;
            Console.WriteLine("Student module system.");
            Console.WriteLine("Welcome {0}", student.LastName);
            do
            {
                Console.WriteLine("Chose what do you want to do:");
                Console.WriteLine("1 - pass test");
                Console.WriteLine("2 - view statistic");
                ConsoleKey taskKey = Console.ReadKey(true).Key;

                switch (taskKey)
                {
                    case ConsoleKey.D1:
                        PassingTest(student);
                        break;
                    case ConsoleKey.D2:
                        StudentStatistic(student);
                        break;
                    default:
                        Console.WriteLine("Not correct task selected. Try again?[Y/any]");
                        ConsoleKeyInfo continueKey = Console.ReadKey(true);
                        if (continueKey.KeyChar == 'Y' || continueKey.KeyChar == 'y' || continueKey.Key == ConsoleKey.Enter)
                        {
                            Console.Clear();
                            continue;
                        }
                        else break;
                }

                isActive = false;
            } while (isActive);
        }

        public static void TeacherCapabilities(UserDataModel teacher)
        {
            bool isActive = true;
            Console.WriteLine("Teacher module system.");
            Console.WriteLine("Welcome {0}", teacher.LastName);
            do
            {
                Console.WriteLine("Chose what do you want to do:");
                Console.WriteLine("1 - create test");
                Console.WriteLine("2 - view statistic");
                ConsoleKey taskKey = Console.ReadKey(true).Key;

                switch (taskKey)
                {
                    case ConsoleKey.D1:
                        CreatingTest(teacher);
                        break;
                    case ConsoleKey.D2:
                        TeacherStatistic(teacher);
                        break;
                    default:
                        Console.WriteLine("Not correct task selected. Try again?[Y/any]");
                        ConsoleKeyInfo continueKey = Console.ReadKey(true);
                        if (continueKey.KeyChar == 'Y' || continueKey.KeyChar == 'y' || continueKey.Key == ConsoleKey.Enter)
                        {
                            Console.Clear();
                            continue;
                        }
                        else break;
                }

                isActive = false;
            } while (isActive);
        }

        private static void CreatingTest(UserDataModel teacher)
        {
            Console.WriteLine("This modul is in development. Please come back later.");
            Console.ReadKey();
        }

        public static void PassingTest(UserDataModel user)
        {
            Console.WriteLine("This modul is in development. Please come back later.");
            Console.ReadKey();
        }

        public static void StudentStatistic(UserDataModel student)
        {
            do
            {
                Console.WriteLine("Which statistic you wish to view?");
                Console.WriteLine("1 - by subject");
                Console.WriteLine("2 - by test");
                Console.WriteLine("3 - all my statistic");
                ConsoleKey taskKey = Console.ReadKey(true).Key;
                StatisticService statService = new StatisticService();
                switch (taskKey)
                {
                    case ConsoleKey.D1: // chose statistic by subject
                        Console.WriteLine("Student statistic by subject");
                        Console.WriteLine("Input subject id:");
                        int subjectId;
                        if (Int32.TryParse(Console.ReadLine(), out subjectId))
                        {
                            SubjectsService sService = SubjectsService.GetInstance();
                            SubjectDataModel subject = sService.GetRecordByID(subjectId);
                            if (subject != null)
                            {
                                List<StudentStatisticDataModel> statistics =
                                    statService.GetStudentStatisticBySubject(student.Id, subjectId);
                                if (statistics != null)
                                {
                                    foreach (var statistic in statistics)
                                        Console.WriteLine(statistic.ToString());
                                }
                                else
                                    Console.WriteLine("Not found any yours statistic by subject.");
                            }
                            else Console.WriteLine("This subject does not exist in db.");
                        }
                        else
                            Console.WriteLine("Inputed subject id is not correct.");
                        break;
                    case ConsoleKey.D2: // chose statistic by test
                        Console.WriteLine("Student statistic by test");
                        Console.WriteLine("Input test id:");
                        int testId;
                        if (Int32.TryParse(Console.ReadLine(), out testId))
                        {
                            TestsService tService = TestsService.GetInstance();
                            TestDataModel test = tService.GetRecordByID(testId);
                            if (test != null)
                            {
                                StudentStatisticDataModel statistic =
                                    statService.GetStudentStatistic(student.Id, testId);
                                if (statistic != null)
                                    Console.WriteLine(statistic.ToString());
                                else
                                    Console.WriteLine("Not found any yours statistic by test.");
                            }
                            else Console.WriteLine("This test does not exist in db.");
                        }
                        else
                            Console.WriteLine("Inputed test id is not correct.");
                        break;
                    case ConsoleKey.D3:
                        Console.WriteLine("All student statistic");
                        List<StudentStatisticDataModel> allStatistics = statService.GetAllStudentStatistics(student.Id);
                        if (allStatistics != null)
                        {
                            foreach (var statistic in allStatistics)
                                Console.WriteLine(statistic.ToString());
                        }
                        else
                            Console.WriteLine("Not found any your statistic. Maybe you never passed test?");
                        break;
                    default:
                        Console.WriteLine("Not correct task selected.");
                        break;
                }

                Console.WriteLine("Do you want try again?[Y/any]");
                ConsoleKeyInfo continueKey = Console.ReadKey(true);
                if (continueKey.KeyChar == 'Y' || continueKey.KeyChar == 'y' || continueKey.Key == ConsoleKey.Enter)
                {
                    Console.Clear();
                    continue;
                }
                else break;
            } while (true);
        }

        public static void TeacherStatistic(UserDataModel teacher)
        {
            int subjectId, studentId, groupId;
            string strSubjectId, strStudentId, strGroupId;
            do
            {
                Console.WriteLine("Which statistic you wish to view?");
                Console.WriteLine("1 - by subject");
                Console.WriteLine("2 - by group and subject");
                Console.WriteLine("3 - by student and subject");
                ConsoleKey taskKey = Console.ReadKey(true).Key;
                StatisticService statService = new StatisticService();
                switch (taskKey)
                {
                    case ConsoleKey.D1: // chose statistic by subject
                        Console.WriteLine("Teacher statistic by subject");
                        Console.WriteLine("Input subject id:");
                        if (Int32.TryParse(Console.ReadLine(), out subjectId))
                        {
                            SubjectsService sService = SubjectsService.GetInstance();
                            SubjectDataModel subject = sService.GetRecordByID(subjectId);
                            if (subject != null)
                            {
                                List<TeacherStatisticDataModel> statistics =
                                    statService.GetTeacherStatisticByTeacherAndSubject(teacher.Id, subjectId);
                                if (statistics != null)
                                {
                                    foreach (var statistic in statistics)
                                        Console.WriteLine(statistic.ToString());
                                }
                                else
                                    Console.WriteLine("Not found any yours statistic by subject.");
                            }
                            else Console.WriteLine("This subject does not exist in db.");
                        }
                        else
                            Console.WriteLine("Inputed subject id is not correct.");
                        break;
                    case ConsoleKey.D2: // chose statistic by group and subject

                        Console.WriteLine("Teacher statistic by group and subject");
                        Console.WriteLine("Input subject id:");
                        strSubjectId = Console.ReadLine();
                        Console.WriteLine("Input group id:");
                        strGroupId = Console.ReadLine();
                        if (Int32.TryParse(strSubjectId, out subjectId) && Int32.TryParse(strGroupId, out groupId))
                        {
                            GroupService gService = GroupService.GetInstance();
                            SubjectsService subjService = SubjectsService.GetInstance();

                            GroupDataModel group = gService.GetGroup(groupId);
                            SubjectDataModel subj = subjService.GetRecordByID(subjectId);

                            if (group != null && subj != null)
                            {
                                List<TeacherStatisticDataModel> statistics =
                                    statService.GetTeacherStatisticByGroupAndSubject(groupId, subjectId);
                                if (statistics != null)
                                    foreach (var item in statistics)
                                        Console.WriteLine(item.ToString());
                                else
                                    Console.WriteLine("Not found any statistic by this subject and group.");
                            }
                            else Console.WriteLine("This test or group does not exist in db.");
                        }
                        else
                            Console.WriteLine("Inputed test id or group id is not correct.");
                        break;
                    case ConsoleKey.D3: // by student and subject
                        Console.WriteLine("Teacher statisctic by student and subject");

                        Console.WriteLine("Input student id:");
                        strStudentId = Console.ReadLine();
                        Console.WriteLine("Input subject id:");
                        strSubjectId = Console.ReadLine();

                        if (Int32.TryParse(strStudentId, out studentId) && Int32.TryParse(strSubjectId, out subjectId))
                        {
                            List<TeacherStatisticDataModel> allStatistics = statService.GetTeacherStatisticByStudentAndSubject(studentId, subjectId);
                            if (allStatistics != null)
                            {
                                foreach (var statistic in allStatistics)
                                    Console.WriteLine(statistic.ToString());
                            }
                            else
                                Console.WriteLine("Not found any statistic.");
                        }
                        else
                            Console.WriteLine("Inputed subject id or student id is not correct.");
                        break;
                    default:
                        Console.WriteLine("Not correct task selected.");
                        break;
                }

                Console.WriteLine("Do you want try again?[Y/any]");
                ConsoleKeyInfo continueKey = Console.ReadKey(true);
                if (continueKey.KeyChar == 'Y' || continueKey.KeyChar == 'y' || continueKey.Key == ConsoleKey.Enter)
                {
                    Console.Clear();
                    continue;
                }
                else break;
            } while (true);
        }

    }
}
