using System;
using StudentSubSystem.TestingClasses;
using StudentSubSystem.UserInterface;
using CommonResourcesLib;

namespace StudentSubSystem
{
    class Program
    {
        static void Main(string[] args)
        {
            
            Console.WriteLine("Wellcome to Student System");
            try
            {
                Questions test = new Questions(DialogConsole.Autorization(), DialogConsole.Setmode(), DialogConsole.SetSubject());
                DialogConsole.Testing(test);               
                DialogConsole.PrintResult(test.Resultstrings);
            }
            catch (OperationCanceledException)
            {
                Console.WriteLine(Errors.UserCanceled);
            }
            catch (NotImplementedException)
            {
                Console.WriteLine("NotImplementedException");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }

            Console.ReadKey();
        }

        //static void FillDataBase()
        //{
        //    SubjectsService ss = SubjectsService.GetInstance();
        //    ss.InsertRecord(new SubjectDataModel(1, "Історія України"));
        //    ss.SubminChanges();

        //    ModulsService ms = ModulsService.GetInstance();
        //    ms.InsertRecord(new ModulDataModel(1, 1, "Модуль 1"));
        //    ms.InsertRecord(new ModulDataModel(2, 1, "Модуль 2"));
        //    ms.SubminChanges();

        //    QuestionsService qs = QuestionsService.GetInstance();
        //    AnswersService ans = AnswersService.GetInstance();

        //    qs.InsertRecord(new QuestionDataModel(1, 2,
        //        "Які зміни в соціально-економічному житті східних слов’ян сприяли утворенню Давньоруської держави?",
        //        1, QuestionType.OneAnswer, 1));
        //    ans.InsertRecord(new AnswerDataModel(1, 1, 1, "Занепад і зникнення віче – органу громадського управління"));
        //    ans.InsertRecord(new AnswerDataModel(2, 1, 3, "Підвищення продуктивності праці в землеробстві, удосконалення ремісничого виробництва.", 100));
        //    ans.InsertRecord(new AnswerDataModel(3, 1, 2, "Запровадженням державної релігії, поширення писаного кодифікованого права."));
        //    ans.InsertRecord(new AnswerDataModel(4, 1, 4, "Розклад родово-общинного ладу, початок класової диференціації."));

        //    qs.InsertRecord(new QuestionDataModel(2, 21,
        //        "Укажіть ознаки кризи в сільському господарстві Наддніпрянської України в першій половині ХІХ ст.",
        //        1, QuestionType.MultipleAnswers, 1));
        //    ans.InsertRecord(new AnswerDataModel(5, 2, 1, "Низька товарність господарства.", 33));
        //    ans.InsertRecord(new AnswerDataModel(6, 2, 3, "Посилення кріпосницького гніту.", 33));
        //    ans.InsertRecord(new AnswerDataModel(7, 2, 2, "Зменшення плати кріпакам за їхню роботу.", 34));
        //    ans.InsertRecord(new AnswerDataModel(8, 2, 6, "Зростання кількості безземельних селян."));
        //    ans.InsertRecord(new AnswerDataModel(9, 2, 4, "Переведення всіх кріпаків із відробіткової ренти на грошову."));
        //    ans.InsertRecord(new AnswerDataModel(10, 2, 5, "Падіння цін на сільськогосподарську продукцію в зв’язку з її перевиробництвом."));

        //    qs.InsertRecord(new QuestionDataModel(3, 51,
        //        "Установіть відповідність між назвами періодів історії України та поняттями й термінами, які потрібно використовувати, характеризуючи їх.",
        //        1, QuestionType.Compliance, 2));
        //    ans.InsertRecord(new AnswerDataModel(11, 3, 1, "Стародавня історія України", 1));
        //    ans.InsertRecord(new AnswerDataModel(12, 3, 2, "Київська Русь за перших князів", 2));
        //    ans.InsertRecord(new AnswerDataModel(13, 3, 3, "Розквіт Київської Русі", 3));
        //    ans.InsertRecord(new AnswerDataModel(14, 3, 4, "Київська Русь за часів роздробленості", 4));
        //    ans.InsertRecord(new AnswerDataModel(15, 3, 5, "Християнізація, «Змієві вали», «тесть Європи», «Руська правда».", 3));
        //    ans.InsertRecord(new AnswerDataModel(16, 3, 6, "Привласнююче господарство, археологічна культура, осілий спосіб життя, Велике розселення слов’ян.", 1));
        //    ans.InsertRecord(new AnswerDataModel(17, 3, 7, "Князівські усобиці, удільніземлі, федеративна монархія, «Правда Ярославичів».", 4));
        //    ans.InsertRecord(new AnswerDataModel(18, 3, 8, "Язичництво, полюддя, уроки, погости", 2));
        //    ans.InsertRecord(new AnswerDataModel(19, 3, 9, "Фільваркове господарство, покозачення, магнат, шляхта."));

        //    qs.InsertRecord(new QuestionDataModel(4, 18, "Унаслідок Першого поділу Речі Посполитої до володінь Австрійської монархії приєднано",
        //        1, QuestionType.TypeAnswer, 2));
        //    ans.InsertRecord(new AnswerDataModel(20, 4, 1, "Східну Галичину", 100));
        //    ans.InsertRecord(new AnswerDataModel(22, 4, 2, "Галичину Східну", 100));
        //    ans.InsertRecord(new AnswerDataModel(23, 4, 3, "Галичину", 70));

        //    qs.InsertRecord(new QuestionDataModel(5, 58, "Установіть послідовність подій суспільного життя XIX cт",
        //        1, QuestionType.Ordering, 2));
        //    ans.InsertRecord(new AnswerDataModel(24, 5, 2, "Видання альманаху «Русалка Дністровая»."));
        //    ans.InsertRecord(new AnswerDataModel(25, 5, 3, "Видання збірки поезій «Кобзар» Т. Шевченка."));
        //    ans.InsertRecord(new AnswerDataModel(26, 5, 1, "Вихід трьох частин «Енеїди» І. Котляревського."));
        //    ans.InsertRecord(new AnswerDataModel(27, 5, 4, "Публікація брошури «Ukraina irredenta» Ю. Бачинського."));

        //    qs.SubminChanges();
        //    ans.SubminChanges();

        //    ProfilesService ps = ProfilesService.GetInstance();
        //    ps.InsertRecord(new ProfileDataModel(1, "Default", 0, 25, 1, 1));
        //    ps.SubminChanges();

        //    AssignService ass = AssignService.GetInstance();
        //    ass.InsertRecord(new AssignDataModel(1, 1, 1, 5, 1));
        //    ass.SubminChanges();
        //}
    }
}
