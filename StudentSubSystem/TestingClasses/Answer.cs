﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CommonClassesLib.DataModels;


namespace StudentSubSystem.TestingClasses
{
    public class Answer: IAnswer
    {
        private int _significance;
        private string _group;
        private int _ordinalNumber;
        private int _usersignificance;
        private string _usergroup;
        private string _text;
        private int _userOrdinalNumber;// походу оно тут вобще не надо. 
        private int _id;
        private int _questionid;

        public Answer(AnswerDataModel dm)
        {
            _significance = dm.Significance;
            _group = dm.Group;
            _ordinalNumber = dm.OrdinalNumber;
            _text = dm.Text;
            _id = dm.ID;
            _questionid = dm.QuestionsID;
            _usersignificance = 0;
            _usergroup = "";
            _userOrdinalNumber = 0;

        }

        
        /// <summary>
        /// Возращает текст ответа
        /// </summary>
        public string Text
        {
            get { return _text; }
        }

        public int UserSignificance
        {
            get
            {
                return _usersignificance;
            }
            set
            {
                _usersignificance = value;
            }
        }
        public int QuestionsID
        {
            get { return _questionid; }
        }
        public string UserGroup
        {
            get
            {
                return _usergroup;
            }
            set
            {
                _usergroup = value;
            }
        }
        public int ID
        { get { return _id; } }
        public int UserOrdinalNumber
        {
            get
            {
                return _userOrdinalNumber;
            }
            set
            {
                _userOrdinalNumber = value;
            }
        }
        public int OrdinalNumber
        {
            get { return _ordinalNumber; }
            set { _ordinalNumber = value; }
        }
        public int Significance
        {
            get { return _significance; }
            set { _significance = value; }
        }

    }
}
