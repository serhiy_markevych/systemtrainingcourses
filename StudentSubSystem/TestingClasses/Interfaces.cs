using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StudentSubSystem.TestingClasses
{
    public interface IAnswer
    {
        /// <summary>
        /// Возращает текст ответа
        /// </summary>
        string Text { get; }
        
        /// <summary>
        /// Пометка ответа пользователя
        /// </summary>
        int UserSignificance { get; set; }
        /// <summary>
        /// Група указаная пользователем
        /// </summary>
        string UserGroup { get; set; }
        /// <summary>
        /// Пользователский номер порядка
        /// </summary>
        int UserOrdinalNumber { get; set; }        
    }

    public interface IQuestion
    {
        /// <summary>
        /// Возращает текст позьзовательского ответа.
        /// </summary>
        string UserAnswer { get; set; }
        /// <summary>
        /// Признак отказа от ответа
        /// </summary>
        bool Cancel { set; get; }
        /// <summary>
        /// Возращает тип вопроса
        /// </summary>
        int Type { get; }        
        /// <summary>
        /// Текст вопроса
        /// </summary>
        string Text { get; }
        /// <summary>
        /// Колекция ответов
        /// </summary>
        List<IAnswer> Answers { get; }
        /// <summary>
        /// подтверждение ввода ответа.
        /// </summary>
        void Confirm();
    }

    public interface IQuestions
    {        
        /// <summary>
        /// признак отсутствия вопросов
        /// </summary>
        bool EOE { get; }
        /// <summary>
        /// Время теста
        /// </summary>
        TimeSpan Time { set; }
        /// <summary>
        /// Возращает случайный неотвеченый вопрос
        /// </summary>
        /// <returns>случайный неотвеченый вопрос</returns>
        IQuestion NextQuestion();        

    }
}
