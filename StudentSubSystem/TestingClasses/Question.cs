﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using CommonClassesLib.DataModels;
using CommonClassesLib;
using CommonClassesLib.Services;

namespace StudentSubSystem.TestingClasses
{
   public class Question: IQuestion
    {
       private string _useranswer;
       private List<Answer> _answers;
       private List<TestAnswerDataModel> _testanswers;// тут буду хранить ответы пользователя. для фильного формирования таблицы при сохранении в методе SetResults()
       private int _bals;
       private bool _answered;
       private string _text;
       private QuestionType _type;
       private int _id;
       private int _ordinalNumber;
       private int _significance;
       private bool _cancel;
       
       public Question(QuestionDataModel qdm, int shuffleAnswers) // создаю экзмемляр вопроса.
       {
           //полностью привожу датамодель к своему типу и дополняю своим функционалом.
           _useranswer = "";
           _id = qdm.ID;
           _ordinalNumber = qdm.OrdinalNumber;
        _significance = qdm.Significance;
           _answered = false;
           _text = qdm.Text;
           _type = qdm.Type;
           _bals = 0;
           _cancel = false;
           _testanswers = new List<TestAnswerDataModel>();
           // далее заполняю этот лист 

           AnswersService ansrvc = AnswersService.GetInstance();
           IEnumerable<AnswerDataModel> ansdm = ansrvc.GetRecords();
           var newlist = (from n in ansdm
                     where n.QuestionsID == _id
                     select new Answer(n)).ToList<Answer>();
           _answers = convertanswerprofile(newlist,shuffleAnswers);// перемешиваю если надо. 

       }

       private List<Answer> convertanswerprofile(IEnumerable<Answer> list, int shuffleAnswers)
       {
           // в этом методе если надо я перемешиваю и возвращаю все или каунт штук.

           List<Answer> resultlist = list.OrderBy(item => item.OrdinalNumber).ToList<Answer>();
           if (shuffleAnswers == 1)// если надо перемешать. перемешиваю.
               suported_classes.To_shuffle<Answer>(resultlist);


           return resultlist;
       }
       /// <summary>
       /// ответ пользователя, "свой вариант"
       /// </summary>
        public string UserAnswer
        {
            get
            {
                return _useranswer;
            }
            set
            {
                _useranswer = value;
            }
        }
       /// <summary>
       /// хранятся балы набраные за вопрос пользователем.
       /// </summary> 
       public int Bals
        {
            get { return _bals; }
        }
        public bool Cancel
        {

            set { _cancel = value; }
            get { return  _cancel;}
        }
        public int Type
        {
            get { return (int)_type; }// c Ваней обсудить почему тут инт ?
        }
        public string Text
        {
            
            get { return _text; }
        }
        public List<TestAnswerDataModel> TestunswerList
        {
            get { return _testanswers; }
        }
        /// <summary>
        /// возвращает состояние вопроса (отвечен/неотвечен)
        /// </summary>
       public bool Answered
        {
            get { return _answered; }
            set { _answered = value; }
        }
       public int ID
       { 
           get 
           {
             return _id;
           }
       }
       public int OrdinalNamber
       { 
           get
           { return _ordinalNumber; }
       }
        public void Confirm()
       {
           _answered = true;
            if (_cancel) return; //если отказался от вопроса. то не начисляю балы и не заношу ответы, ибо их нет.
            //что если cancel? вроде все будет хорошо.
            //получить контекст. не забыть освободить потом. 
            TestAnswersService tasrvs = TestAnswersService.GetInstance();
           switch (_type)// cчитаю оценку.
           {
               case 0:
                   {
                       if (_answers.Count(item => item.UserSignificance == item.Significance) == 1)//значит правильнй ответ
                           _bals = _significance;
                       Answer answer =  _answers.FirstOrDefault(item => item.UserSignificance==1);
                       TestAnswerDataModel tadm = new TestAnswerDataModel();
                       tadm.ID = 0;
                       tadm.OrdinalNumber = answer.OrdinalNumber;
                       tadm.AnswersID = answer.ID;
                       tadm.TestQuestionsID = ID;
                      // tadm.Text= заполняется только для 6го типа вопросов.
                       _testanswers.Add(tadm);
                       break;
                   }
           }
           
       }

      public  List<IAnswer> Answers
        {
            get { return _answers.ToList<IAnswer>(); }
        }
    }
}
