﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AuthenticationLib;


using CommonClassesLib.DataModels;
using CommonClassesLib;
using CommonClassesLib.Services;

namespace StudentSubSystem.TestingClasses
{
    public class Questions: IQuestions // недостощие описания лежат в описание интерфейса.
    {
        #region private
        private int _profileid;
       // private int _studid;
        private List<Question> _questions;
        private TimeSpan _time;
        private int _curentindex;
       // private bool _eoe;
        private DateTime _timebegan;
        private DateTime _timeend;
        private bool _timebeganed;// если тру. то время уже было засечено. 
        private List<string> _resultstrings;
        private TestDataModel _test;
        private UserDataModel _user;
        private TestMode _mode;
        private int _idsubject;
        private TimeSpan _time_limit;
        #endregion private
        
        private List<Question> convertquestionprofile(IEnumerable<Question> list, int shuffleQuestions, int count)
        {
            // в этом методе если надо я перемешиваю и возвращаю все или каунт штук.

            List<Question> resultlist = list.OrderBy(item=>item.OrdinalNamber).ToList<Question>();
            if (shuffleQuestions == 1)// если надо перемешать. перемешиваю.
            suported_classes.To_shuffle<Question>(resultlist);


            if ( count!=0 && resultlist.Count > count)// если в колекции больше елементов чем надо. вырезаю сколько надо. 
            {
                List<Question> newlist = new List<Question>();
                for (int i = 0; i < count; i++)
                    newlist.Add(resultlist[i]);

                resultlist = newlist; //перенаправляю сылку. 
            }

            return resultlist;
        }

        public Questions(UserDataModel user, TestMode mode, int idsubject)
        {
            _user = user;
            _mode = mode;
            _idsubject = idsubject;            
            _timebeganed = false;
            _resultstrings = new List<string>();
            _test = null;
            _curentindex = 0;

            //1. согласно группе и id предмета получаю нужный профайл настроек.
            AssignService assrvs = AssignService.GetInstance(); //получил сылку на контекст
            AssignDataModel assdm = assrvs.GetRecordBySubjectsIDGroupsID(_idsubject,_user.Group.Id);

            ProfilesService prfsrvs = ProfilesService.GetInstance(); //получил сылку на контекст
            ProfileDataModel prfdm = prfsrvs.GetRecordByID(assdm.ProfilesID);
            _profileid = prfdm.ID;

            Time_limit =TimeSpan.FromMinutes(prfdm.TimeLimit);
            ModulsService mdsrvs = ModulsService.GetInstance();
            IEnumerable<ModulDataModel> mddm = mdsrvs.GetRecordsBySubjectsID(_idsubject);

            QuestionsService quesrvs = QuestionsService.GetInstance();
            IEnumerable<QuestionDataModel> quedm = quesrvs.GetRecords();

            // выбор всех вопросов без учета настроек профайла.
            //тут же привожу его к нужному мне типу вопроса.
            IEnumerable<Question> res = null;
            if (prfdm.ModulsID == null)//беру все записи для вех совпадений 
            {
                 res =
                        from m in mddm
                        join p in quedm on m.ID equals p.ModulsID
                        select new Question(p,prfdm.ShuffleAnswers);
            }
            else // выбираю только те записи что попали для конкретного ModulsID
            { 
                res =
                    from q in quedm
                    where prfdm.ModulsID == q.ModulsID
                    select new Question(q, prfdm.ShuffleAnswers);
            }
            // возвращаю в лист уже нужное количиство вопросов и перемешав их.
            _questions = convertquestionprofile(res,prfdm.ShuffleQuestions,prfdm.QuestionsCount); 
        }
        private int Gettotalanswers()
        {
            return _questions.Count(item => item.Bals == 1);//
        }
        private int GettotalRating()
        {
            return _questions.Count(item => item.Bals == 1);//
        }
         
        public void SetResults()//вычисляет результат и сохраняет все в базу. 
        {
            _time = _timeend - _timebegan;
            if (_time > _time_limit) { /*пользоваетль не вложился.*/}
            // формирую колекцию результатов для вани.
            for (int i = 0; i < _questions.Count(); i++)
            {
                _resultstrings.Add(string.Format( "вопрос №{0}:{1}; вы получили за него {2} балов",i,_questions[i].Text,_questions[i].Bals)); 
            }

            // формирую класс Тест
            TestsService tsrvc = TestsService.GetInstance();
          _test = new TestDataModel();
            //далее заполняю все поля для сохранения
          _test.ID = 0;
          _test.Mode = _mode;
          _test.ProfilesID = _profileid;
          _test.StudentID = _user.Id;
          _test.SubjectsID = _idsubject;
          _test.TotalAnswers = Gettotalanswers();  
          _test.TotalQuestions = _questions.Count();
          _test.Rating = 0; 
          _test.TotalRating = GettotalRating();
          _test.Date = _timebegan;
          _test.Duration = _time;
          _test.DurationTicks = _time.Ticks;             
          tsrvc.InsertRecord(_test);
          tsrvc.SubminChanges();
            //теперь тест сохранен. но необходимо сохранить и таблицы TestQuestionDataModel и TestAnswerDataModel
          TestQuestionsService tqsrvs = TestQuestionsService.GetInstance();
          TestAnswersService tasrvs = TestAnswersService.GetInstance();
           foreach (var q in _questions)
          {
              TestQuestionDataModel tqdm = new TestQuestionDataModel();
              tqdm.ID = 0;
              tqdm.TestsID = _test.ID;
              tqdm.QuestionsID = q.ID;
              tqdm.OrdinalNumber = q.OrdinalNamber;
              tqsrvs.InsertRecord(tqdm);

              foreach (TestAnswerDataModel t in q.TestunswerList)
              {
                  tasrvs.InsertRecord(t);

              }
              tasrvs.SubminChanges();
              tqsrvs.SubminChanges();
          }

        }
        
        #region public properties
        public bool EOE
        {
            get
            {
                if (_questions.Count(item => !item.Answered) == 0)
                {
                    _timeend = DateTime.Now;
                   
                    // вызываю метод анализа теста и сохраниеняю данные в базу
                    SetResults();
                    return true;
                }
                return false;
            }
        }
        public List<string> Resultstrings // поле в котором содержаться результаты теста. 
        {
            get
            {//если ответы еще не готовы а кто то дергент этот метод.
                if (_resultstrings == null) throw new Exception("вы не можете забрать ответы, колекция еще не сформирована.");
                return _resultstrings;
            }
        }
        public TimeSpan Time
        {
            set { _time = value;} 
            get {return _time;} 
        }
        public TimeSpan Time_limit
        {
            set { _time_limit = value; }
            get { return _time_limit; }
        }
        #endregion public properties
        public IQuestion NextQuestion()
        {//метка. возможно будет вернее использовать тут класс очередь. но тогда механика методов измениться.
            //засекаю время. 
            if (!_timebeganed){ _timebegan = DateTime.Now; _timebeganed = true;}
            
            // возвращаю следующий вопрос. для этого :
            //1. начинаю прогонять колекцию от curentindex до первого ответа != answered
            //2. найдя такой элемент. возвращаю его а curentindex присваиваю номер найденого+1. 
            //3. дойдя до конца колекции curentindex = 0. и начинаю поиск сначала.
           int index = _curentindex;
            while (true)
            {
               if (index >_questions.Count()-1) index = 0; // проверка на случай если вышел за размер масива. и начинаю заново поиск.
               if ( _questions[index].Answered != true )
               {
                   _curentindex++;
                   return _questions[index];
               }
               else
               {
                   index++;
               }
            }            
            //_questions.FirstOrDefault(item => !item.Answered);
        }
    }
}
