﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StudentSubSystem.TestingClasses
{
   public static class suported_classes
    {
       /// <summary>
       /// перемешивает любую колекцию.
       /// </summary>
       public static void To_shuffle<T>(List<T> list)
       {
           
           if (list == null) throw new Exception("Передана пустая колекция");//локализация ??

           int lenght = list.Count();

           Random rnd = new Random();
           int k = 0;
           for (int i = 0; i < lenght / 2; i++)
           {
               k = rnd.Next(0, lenght - 1);
               T t = list[k];
               list[k] = list[i];
               list[i] = t;
           }

       }
    }
}
