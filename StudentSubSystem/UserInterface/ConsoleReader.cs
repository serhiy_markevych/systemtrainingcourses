using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CommonResourcesLib;

namespace StudentSubSystem.UserInterface
{
    public class ConsoleReader
    {        
        private static void CanceledOperation()
        {
            Console.WriteLine(Messages.Cancel);
            if (DialogConsole.OkNo()) throw new OperationCanceledException();
        }

        private static ConsoleKeyInfo GetKey()
        {
            repeat:
            ConsoleKeyInfo key = Console.ReadKey();
            if (key.Key == ConsoleKey.Escape) { if (!DialogConsole.OkNo())goto repeat; Console.WriteLine(); ConsoleReader.CanceledOperation(); }
            return key;
        }

        /// <summary>
        /// Getting char
        /// </summary>
        /// <returns></returns>
        public static char GetChar() { return GetKey().KeyChar; }

        /// <summary>
        /// Geting Digit
        /// </summary>
        /// <returns></returns>
        public static byte GetDigit()
        {
            ConsoleKeyInfo key;
            while (true)
            {
                key = GetKey();
                if (key.Key == ConsoleKey.Enter) return 13;               
                if (key.Key == ConsoleKey.Spacebar) return 32;            
                if (key.Key == ConsoleKey.D0) return 0;                  
                if (!char.IsDigit(key.KeyChar))
                    Console.WriteLine(Errors.Value);
                else
                    return Convert.ToByte(key.KeyChar.ToString());
            } 
        }
        /// <summary>
        /// Entering String
        /// </summary>
        /// <returns>Строка пароля</returns>
        public static string EnterString(bool security)
        {
            ConsoleKeyInfo key = Console.ReadKey(security);
            StringBuilder str = new StringBuilder();
            while (key.Key != ConsoleKey.Enter && key.Key != ConsoleKey.Escape)
            {
                if (key.Key == ConsoleKey.Backspace)
                {
                    if (str.Length > 0)
                    {
                        str.Remove(str.Length - 1, 1);
                        Console.Write(' ');
                        Console.CursorLeft--;
                    }
                    else
                        Console.CursorLeft++;
                }
                else
                    str.Append(key.KeyChar);
                key = Console.ReadKey(security);
            }
            if (key.Key == ConsoleKey.Escape) CanceledOperation();
            return str.ToString();
        }
    }
}
