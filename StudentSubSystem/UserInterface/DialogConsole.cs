using System;
using System.Collections.Generic;
using System.Linq;
using AuthenticationLib;
using CommonClassesLib;
using CommonClassesLib.DataModels;
using CommonClassesLib.Services;
using CommonResourcesLib;
using StudentSubSystem.TestingClasses;
using StudentSubSystem.UserInterface.HandlersQuestion;

namespace StudentSubSystem.UserInterface
{
    public class DialogConsole
    {
        private static UserDataModel _user;  
       
        private static List<SubjectDataModel> GetSubjects()
        {
            if (_user == null) throw new NullReferenceException(Errors.NotUser);
            SubjectsService subjectservice = SubjectsService.GetInstance();
            AssignService asignservice = AssignService.GetInstance();            
            var asigns = asignservice.GetRecords().Where(wh => wh.GroupsID == _user.Group.Id);
            if (asigns == null) throw new NullReferenceException(Errors.NotGroup);
            var subjects = from subject in subjectservice.GetRecords()
                           from asign in asigns
                           where asign.SubjectsID == subject.ID
                           select subject;           
            return subjects.ToList<SubjectDataModel>(); 
        }
        
        #region PublicMetod
        /// <summary>
        /// Method confirm
        /// </summary>
        /// <returns></returns>
        public static bool OkNo()
        {
            while (true)
            {
                Console.Write(Messages.Confirm);
                ConsoleKeyInfo answer = Console.ReadKey();
                Console.WriteLine();
                if ((answer.Key == ConsoleKey.Y) || (answer.Key == ConsoleKey.N))
                {
                    Console.Clear();
                    return (answer.Key == ConsoleKey.Y);
                }
                Console.WriteLine("\n" + Errors.Value);
            }
        }

        /// <summary>
        /// Method Autorization
        /// </summary>
        /// <returns></returns>
        public static UserDataModel Autorization()
        {
            Console.Clear();
            do
            {
                Console.Write(Messages.Invite + "\n" + Messages.Login + ":");
                string login = ConsoleReader.EnterString(false);
                Console.Write("\n" + Messages.Invite + "\n" + Messages.Password + ":");
                string password = ConsoleReader.EnterString(true);
                UserDataModel user = Authentication.Login(login, password);
                if (user != null) { Console.Clear(); _user = user; return user; }
                Console.WriteLine(Errors.Login);
                Console.WriteLine(Messages.Repeat);
            }
            while (OkNo());
            return null;
        }              

        /// <summary>
        /// Method set mode testing
        /// </summary>
        /// <returns>Возращает режим тестирования</returns>
        public static TestMode Setmode()
        {
            Console.Clear();
            Console.WriteLine(Messages.InviteMode);
            Console.WriteLine(Messages.ModeLearning + "\n" + Messages.ModeTesting);
            while (true)
            {
                Console.Write(Messages.InviteKey + ":");                
                try
                {
                    switch (ConsoleReader.GetChar())
                    {
                        case '1': { Console.Clear(); return TestMode.Learning; }
                        case '2': { Console.Clear(); return TestMode.Testing; }
                        default: Console.WriteLine(Errors.ValueChanged); break;
                    }
                }
                catch (Exception) { Console.WriteLine(Errors.Error); }
            }
        }

        public static int SetSubject()
        {
            List<SubjectDataModel> subjects = GetSubjects();
            if (subjects == null || subjects.Count == 0) throw new NullReferenceException(Errors.NotGroup);
            int number = -1;
            Console.Clear();
            foreach (var item in subjects)
                Console.WriteLine(item.ID.ToString() + "\t" + item.Name);
            Console.WriteLine();
            do
            {
                Console.WriteLine(Messages.SetTest);
                string snumber = ConsoleReader.EnterString(false);
                try { number = Convert.ToInt32(snumber); }
                catch (Exception) { Console.WriteLine(snumber + Errors.Value); continue; }
                SubjectDataModel temp = subjects.FirstOrDefault(sel => sel.ID == number);
                if (temp != null) { Console.Clear(); return temp.ID; }
                else Console.WriteLine(snumber + Errors.ValueChanged);
            } while (true);
        }

        /// <summary>
        /// Look testing
        /// </summary>
        /// <param name="test">Обект с данными по тесту</param>
        public static void Testing(IQuestions test)
        {
            if (test == null) throw new NullReferenceException(Errors.NotTest);
            if (test.EOE) throw new NullReferenceException(Errors.NotQustions);
            Console.WriteLine(Messages.StartTesting); Console.ReadKey(); Console.Clear();            
            while (!test.EOE)
                FactoryHandlerQuestion.CreateHandlerQuestion(test.NextQuestion()).HandlerQuestionWork();            
            Console.WriteLine(Messages.EndTesting);
        }

        public static void PrintResult(List<string> results)
        {
            Console.WriteLine(Messages.TestTime);
            foreach (var item in results)
                Console.WriteLine(item);            
        }
        #endregion
       
    }
}
