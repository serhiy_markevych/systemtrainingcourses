using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using StudentSubSystem.TestingClasses;
using CommonResourcesLib;

namespace StudentSubSystem.UserInterface.HandlersQuestion
{
    class ComplianceHandler:HandlerQuestion
    {
        public ComplianceHandler(IQuestion question) : base(question) { }
        protected override string TextUserAnswer(IAnswer answer)
        {
            return (answer.UserSignificance == 0 ? " " : answer.UserSignificance.ToString());
        }
        public override void HandlerQuestionWork()
        {             
            PrintAnswer();
            while (true)
            {
                byte number; byte grupa;          
                Console.Write("\n" + Messages.InviteGroup + ":"); grupa = ConsoleReader.GetDigit();
                var temp = _question.Answers.Where(w => w.UserSignificance == grupa);
                if (temp.Count() == 2)
                {
                    Console.Write("\n" + Messages.ResetGroup);
                    if (DialogConsole.OkNo()) foreach (var item in temp) item.UserSignificance = 0;
                    else { PrintAnswer(); continue; } PrintAnswer();
                    Console.Write("\n" + Messages.InviteGroup + ":" + grupa.ToString());
                }
                Console.Write("\n" + Messages.InviteNumber + ":"); number = ConsoleReader.GetDigit();
                if (number == 32) { _question.Cancel = true; _question.Confirm(); break; }
                if (number == 0) break;
                if ((number != 0) && (number <= _question.Answers.Count))
                {
                    _question.Answers[number - 1].UserSignificance = grupa;
                    PrintAnswer();
                }
                else Console.WriteLine(Environment.NewLine + Errors.ValueChanged);
                if (_question.Answers.Count(cnt => cnt.UserSignificance == 0) == 0)
                {
                    Console.WriteLine("\n" + Messages.AnswerConfirm);
                    if (DialogConsole.OkNo()) { _question.Confirm(); break; }
                    else
                    {
                        foreach (var item in _question.Answers) item.UserSignificance = 0;
                        PrintAnswer();
                    }
                }
            }
        }
    }
}
