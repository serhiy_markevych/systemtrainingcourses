using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using StudentSubSystem.TestingClasses;
using CommonResourcesLib;

namespace StudentSubSystem.UserInterface.HandlersQuestion
{
    static class FactoryHandlerQuestion
    {
        public static HandlerQuestion CreateHandlerQuestion(IQuestion question)
        {
            if (question == null) throw new NullReferenceException(Errors.NotTest);
            switch (question.Type)
            {
                case 0: return new OneAnswerHandler(question);
                case 1: return new ManyAnswerHandler(question);
                case 2: return new TypeAnswerHandler(question);
                case 3: return new ComplianceHandler(question);
                case 4: return new OrderingHandler(question);
                case 5: return new ClassificationHandler(question);
                default: throw new NotImplementedException();
            }
        }
    }
}
