﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CommonResourcesLib;
using StudentSubSystem.TestingClasses;

namespace StudentSubSystem.UserInterface.HandlersQuestion
{
    public class HandlerQuestion
    {        
        protected IQuestion _question;
        public HandlerQuestion(IQuestion question) { this._question = question; }

        protected virtual string TextUserAnswer(IAnswer answer) { return ""; }
        public virtual void HandlerQuestionWork() { }
        
        protected void PrintAnswer()
        {
            Console.Clear();            
            Console.WriteLine("[{0}]", Messages.AnswerConfirm);
            Console.WriteLine(_question.Text + "\n");
            for (int i = 0; i < _question.Answers.Count; i++)
                Console.WriteLine("{0}\t{1}. " + _question.Answers[i].Text, this.TextUserAnswer(_question.Answers[i]), i+1);                   
        }
    }
}
