using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using StudentSubSystem.TestingClasses;
using CommonResourcesLib;

namespace StudentSubSystem.UserInterface.HandlersQuestion
{
    public class ManyAnswerHandler : HandlerQuestion
    {
        public ManyAnswerHandler(IQuestion question) : base(question) { }
        protected override string TextUserAnswer(IAnswer answer)
        {
            return (answer.UserSignificance == 1 ? "*" : " ");
        }
        public override void HandlerQuestionWork()
        {           
            PrintAnswer();
            while (true)
            {
                byte number;
                Console.WriteLine("[{0}]", Messages.AnswerInvate);
                number = ConsoleReader.GetDigit();
                if (number == 13) { _question.Confirm(); break; }
                if (number == 32) { _question.Confirm(); _question.Cancel = true; break; }
                if (number == 0) break;
                if ((number != 0) && (number <= _question.Answers.Count))
                {
                    if (_question.Answers[number - 1].UserSignificance == 1)
                        _question.Answers[number - 1].UserSignificance = 0;
                    else
                        _question.Answers[number - 1].UserSignificance = 1;
                    PrintAnswer();
                }
                else Console.WriteLine(Errors.ValueChanged);
            }
        }
    }
}
