using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using StudentSubSystem.TestingClasses;
using CommonResourcesLib;

namespace StudentSubSystem.UserInterface.HandlersQuestion
{
    class OneAnswerHandler : HandlerQuestion
    {
        public OneAnswerHandler(IQuestion question):base(question){}
        protected override string TextUserAnswer(IAnswer answer)
        {
            return (answer.UserSignificance == 1 ? "*" : " ");
        }
        public override void HandlerQuestionWork()
        {                   
            PrintAnswer();
            while (true) 
            {
                byte number;    
                Console.WriteLine("[{0}]", Messages.AnswerInvate);
                number = ConsoleReader.GetDigit();
                if (number == 13) { _question.Confirm(); break; }
                if (number == 32) { _question.Cancel = true; _question.Confirm(); break; }
                if (number == 0) break;
                if ((number != 0) && (number <= _question.Answers.Count))
                {
                    IAnswer temp = _question.Answers.SingleOrDefault(sel => sel.UserSignificance == 1);
                    if (temp != null) temp.UserSignificance = 0;
                    _question.Answers[number - 1].UserSignificance = 1;
                    PrintAnswer();
                }
                else Console.WriteLine(Errors.ValueChanged);
            }           
        }
    }
}
