using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using StudentSubSystem.TestingClasses;
using CommonResourcesLib;

namespace StudentSubSystem.UserInterface.HandlersQuestion
{
    class OrderingHandler:HandlerQuestion
    {
        public OrderingHandler(IQuestion question) : base(question) { }
        protected override string TextUserAnswer(IAnswer answer)
        {
            return (answer.UserSignificance == 0 ? " " : answer.UserSignificance.ToString());
        }
        public override void HandlerQuestionWork()
        {            
            PrintAnswer();
            byte number; byte grupa = 1;
            while (true)
            {
                
                Console.WriteLine("[{0}]", Messages.AnswerInvate);
                number = ConsoleReader.GetDigit();
                if (number == 32) { _question.Cancel = true; _question.Confirm(); break; }
                if (number == 0) break;
                if ((number != 0) && (number <= _question.Answers.Count))
                {
                    if (grupa > _question.Answers.Count)
                        grupa = 1;
                    _question.Answers[number - 1].UserSignificance = grupa++;
                    PrintAnswer();
                }
                else Console.WriteLine(Errors.ValueChanged);
                if (_question.Answers.Count(cnt => cnt.UserSignificance == 0) == 0)
                {
                    Console.WriteLine("\n" + Messages.AnswerConfirm);
                    if (DialogConsole.OkNo()) { _question.Confirm(); break; }
                    else
                    {
                        foreach (var item in _question.Answers) item.UserSignificance = 0;
                        grupa = 1;
                        PrintAnswer();
                    }
                }
            }
           
        }
    }
}
