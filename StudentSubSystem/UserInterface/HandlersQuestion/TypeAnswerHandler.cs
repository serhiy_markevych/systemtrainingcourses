﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using StudentSubSystem.TestingClasses;
using CommonResourcesLib;

namespace StudentSubSystem.UserInterface.HandlersQuestion
{
    class TypeAnswerHandler:HandlerQuestion
    {
        public TypeAnswerHandler(IQuestion question) : base(question) { }
        protected override string TextUserAnswer(IAnswer answer)
        {
            return "User answer: '" + _question.UserAnswer + "'\n";
        }
        public override void HandlerQuestionWork()
        {
            string text="";
            do
            {
                Console.Clear();
                Console.WriteLine(_question.Text);
                Console.WriteLine(Messages.YourAnswer + "\n");
                text = ConsoleReader.EnterString(false);
                Console.WriteLine("\n" + Messages.AnswerConfirm);
            } while (!DialogConsole.OkNo());
            _question.UserAnswer = text;
            _question.Confirm();
        }
    }
}
