﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AuthenticationLib;
using CommonClassesLib.DataModels;
using CommonClassesLib.Services;
using CommonClassesLib;

namespace TeacherSubSystem
{
    public static class DisplayMenu
    {
        public static int UserID { get; set; }

        public static void Show()
        {
            Console.Write("Введите логин: ");
            string login = Console.ReadLine();
            Console.Write("Введите пароль: ");
            string pass = Console.ReadLine();

            try
            {
                UserDataModel user = Authentication.Login(login, pass);
                UserID = user.Id;
                if (user.Roles.FirstOrDefault().Equals(Role.Teacher))
                {
                    LoadMenu.LoadMainMenu();
                }
                else Console.WriteLine("Неверная роль");
            }
            catch
            {
                Console.WriteLine("Ошибка авторизации");
            }
        }

    }
}
