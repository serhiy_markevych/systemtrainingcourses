﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Configuration;
using CommonClassesLib.Services;
using CommonClassesLib.DataModels;
using CommonClassesLib;

namespace TeacherSubSystem
{
    public static class LoadMenu
    {
        public static void LoadMainMenu()
        {
            Console.Clear();
            Console.WriteLine("        *** Главное меню ***");
            Console.WriteLine("+++++++++++++++++++++++++++++++++++++\n|");
            Console.WriteLine("| 1 - Перейти к *Предметам*");
            Console.WriteLine("| 2 - Перейти к *Профилям*");
            Console.WriteLine("|\n+++++++++++++++++++++++++++++++++++++");
            Console.WriteLine("| Для выхода из программы введите q");
            Console.WriteLine("+++++++++++++++++++++++++++++++++++++");
            Console.Write("| Сделайте выбор: ");
            string menuID = Console.ReadLine();
            switch (menuID)
            {
                case "1":
                    {
                        Console.Clear();
                        LoadSubjectsMenu();
                    }
                    break;
                case "2":
                    {
                        Console.Clear();
                        LoadProfilesMenu();
                    }
                    break;
                case "q":
                    {
                        Environment.Exit(0);
                    }
                    break;
                default:
                    {
                        Console.Clear();
                        Console.WriteLine("Неправильный ввод! Введите номер меню еще раз.");
                        Console.WriteLine("Для продолжения нажмите любую клавишу...");
                        Console.ReadKey();
                        LoadMainMenu();
                    }
                    break;
            }
        }
        public static void LoadSubjectsMenu()
        {
            Console.Clear();
            SubjectRequests subjRequest = new SubjectRequests();
            Console.WriteLine("       *** Меню -Предметы- ***");
            Console.WriteLine("+++++++++++++++++++++++++++++++++++++\n|");
            Console.WriteLine("| 1 - Просмотр предметов");
            Console.WriteLine("| 2 - Добавить предмет");
            Console.WriteLine("| 3 - Редактировать предмет");
            Console.WriteLine("| 4 - Удалить предмет\n|");
            Console.WriteLine("| 5 - Перейти к *Модулям*\n|");
            Console.WriteLine("| 9 - Вернуться назад");
            Console.WriteLine("|\n+++++++++++++++++++++++++++++++++++++");
            Console.WriteLine("| Для выхода из программы нажмите q");
            Console.WriteLine("+++++++++++++++++++++++++++++++++++++");
            Console.Write("| Сделайте выбор: ");
            string menuID = Console.ReadLine();
            switch (menuID)
            {
                case "1":
                    {
                        Console.Clear();
                        subjRequest.GetSubjectRequest();
                        Console.WriteLine("Для продолжения нажмите любую клавишу...");
                        Console.ReadKey();
                        LoadSubjectsMenu();
                    }
                    break;
                case "2":
                    {
                        Console.Clear();
                        subjRequest.AddSubjectRequest();
                        Console.WriteLine("Для продолжения нажмите любую клавишу...");
                        Console.ReadKey();
                        LoadSubjectsMenu();
                    }
                    break;
                case "3":
                    {
                        Console.Clear();
                        subjRequest.EditSubjectRequest();
                        Console.WriteLine("Для продолжения нажмите любую клавишу...");
                        Console.ReadKey();
                        LoadSubjectsMenu();
                    }
                    break;
                case "4":
                    {
                        Console.Clear();
                        subjRequest.RemoveSubjectRequest();
                        Console.WriteLine("Для продолжения нажмите любую клавишу...");
                        Console.ReadKey();
                        LoadSubjectsMenu();
                    }
                    break;
                case "5":
                    {
                        try
                        {
                            Console.Clear();
                            subjRequest.GetSubjectRequest();
                            Console.Write("Введите id предмета: ");
                            int subj_id = int.Parse(Console.ReadLine());
                            LoadModulsMenu(subj_id);
                        }
                        catch (FormatException)
                        {
                            Console.WriteLine("Неправильный ввод! Введите id еще раз.");
                            Console.WriteLine("Для продолжения нажмите любую клавишу...");
                            Console.ReadKey();
                            LoadSubjectsMenu();
                        }
                    }
                    break;
                case "6":
                    {
                        //LoadAssignMenu();
                    }

                    break;
                case "9":
                    {
                        LoadMainMenu();
                    }
                    break;
                case "q":
                    {
                        Environment.Exit(0);
                    }
                    break;
                default:
                    {
                        Console.Clear();
                        Console.WriteLine("Неправильный ввод! Введите номер меню еще раз.");
                        Console.WriteLine("Для продолжения нажмите любую клавишу...");
                        Console.ReadKey();
                        LoadSubjectsMenu();
                    }
                    break;
            }
        }
        public static void LoadProfilesMenu()
        {
            Console.Clear();
            ProfileRequests profRequest = new ProfileRequests();
            Console.WriteLine("       *** Меню -Профили- ***");
            Console.WriteLine("+++++++++++++++++++++++++++++++++++++\n|");
            Console.WriteLine("| 1 - Просмотр профилей");
            Console.WriteLine("| 2 - Добавить профиль");
            Console.WriteLine("| 3 - Редактировать профиль");
            Console.WriteLine("| 4 - Удалить профиль\n|");
            Console.WriteLine("| 5 - Перейти к *Шкале оценок*\n|");
            Console.WriteLine("| 9 - Вернуться назад");
            Console.WriteLine("|\n+++++++++++++++++++++++++++++++++++++");
            Console.WriteLine("| Для выхода из программы нажмите q");
            Console.WriteLine("+++++++++++++++++++++++++++++++++++++");
            Console.Write("| Сделайте выбор: ");
            string menuID = Console.ReadLine();
            switch (menuID)
            {
                case "1":
                    {
                        Console.Clear();
                        profRequest.GetProfileRequest();
                        Console.WriteLine("Для продолжения нажмите любую клавишу...");
                        Console.ReadKey();
                        LoadProfilesMenu();
                    }
                    break;
                case "2":
                    {
                        Console.Clear();
                        profRequest.AddProfileRequest();
                        Console.WriteLine("Для продолжения нажмите любую клавишу...");
                        Console.ReadKey();
                        LoadProfilesMenu();
                    }
                    break;
                case "3":
                    {
                        Console.Clear();
                        profRequest.EditProfileRequest();
                        Console.WriteLine("Для продолжения нажмите любую клавишу...");
                        Console.ReadKey();
                        LoadProfilesMenu();
                    }
                    break;
                case "4":
                    {
                        Console.Clear();
                        profRequest.RemoveProfileRequest();
                        Console.WriteLine("Для продолжения нажмите любую клавишу...");
                        Console.ReadKey();
                        LoadProfilesMenu();
                    }
                    break;
                case "5":
                    {
                        Console.Clear();
                        profRequest.GetProfileRequest();
                        Console.Write("Введите id профиля: ");
                        try
                        {
                            int prof_id = int.Parse(Console.ReadLine());
                            LoadRatingScaleMenu(prof_id);
                        }
                        catch (FormatException)
                        {
                            Console.WriteLine("Неправильный ввод! Введите id еще раз.");
                            Console.WriteLine("Для продолжения нажмите любую клавишу...");
                            Console.ReadKey();
                            LoadProfilesMenu();
                        }
                    }
                    break;
                case "9":
                    {
                        LoadMainMenu();
                    }
                    break;
                case "q":
                    {
                        Environment.Exit(0);
                    }
                    break;
                default:
                    {
                        Console.Clear();
                        Console.WriteLine("Неправильный ввод! Введите номер меню еще раз.");
                        Console.WriteLine("Для продолжения нажмите любую клавишу...");
                        Console.ReadKey();
                        LoadProfilesMenu();
                    }
                    break;
            }
        }
        public static void LoadRatingScaleMenu(int prof_id)
        {
            Console.Clear();
            RatingScaleRequests rsRequest = new RatingScaleRequests();
            Console.WriteLine("     *** Меню -Шкала оценок- ***");
            Console.WriteLine("+++++++++++++++++++++++++++++++++++++\n|");
            Console.WriteLine("| 1 - Просмотр шкалы");
            Console.WriteLine("| 2 - Добавить шкалу");
            Console.WriteLine("| 3 - Редактировать шкалу");
            Console.WriteLine("| 4 - Удалить шкалу\n|");
            Console.WriteLine("| 9 - Вернуться назад");
            Console.WriteLine("|\n+++++++++++++++++++++++++++++++++++++");
            Console.WriteLine("| Для выхода из программы нажмите q");
            Console.WriteLine("+++++++++++++++++++++++++++++++++++++");
            Console.Write("| Сделайте выбор: ");
            string menuID = Console.ReadLine();
            switch (menuID)
            {
                case "1":
                    {
                        Console.Clear();
                        rsRequest.GetRatingScaleRequest(prof_id);
                        Console.WriteLine("Для продолжения нажмите любую клавишу...");
                        Console.ReadKey();
                        LoadRatingScaleMenu(prof_id);
                    }
                    break;
                case "2":
                    {
                        Console.Clear();
                        rsRequest.AddRatingScaleRequest(prof_id);
                        Console.WriteLine("Для продолжения нажмите любую клавишу...");
                        Console.ReadKey();
                        LoadRatingScaleMenu(prof_id);
                    }
                    break;
                case "3":
                    {
                        Console.Clear();
                        rsRequest.EditRatingScaleRequest(prof_id);
                        Console.WriteLine("Для продолжения нажмите любую клавишу...");
                        Console.ReadKey();
                        LoadRatingScaleMenu(prof_id);
                    }
                    break;
                case "4":
                    {
                        Console.Clear();
                        rsRequest.RemoveRatingScaleRequest(prof_id);
                        Console.WriteLine("Для продолжения нажмите любую клавишу...");
                        Console.ReadKey();
                        LoadRatingScaleMenu(prof_id);
                    }
                    break;
                case "9":
                    {
                        LoadProfilesMenu();
                    }
                    break;
                case "q":
                    {
                        Environment.Exit(0);
                    }
                    break;
                default:
                    {
                        Console.Clear();
                        Console.WriteLine("Неправильный ввод! Введите номер меню еще раз.");
                        Console.WriteLine("Для продолжения нажмите любую клавишу...");
                        Console.ReadKey();
                        LoadRatingScaleMenu(prof_id);
                    }
                    break;
            }
        }
        public static void LoadModulsMenu(int subj_id)
        {
            Console.Clear();
            ModuleRequests modulsRequest = new ModuleRequests();
            Console.WriteLine("       *** Меню -Модуль- ***");
            Console.WriteLine("+++++++++++++++++++++++++++++++++++++\n|");
            Console.WriteLine("| 1 - Просмотр модулей");
            Console.WriteLine("| 2 - Добавить модуль");
            Console.WriteLine("| 3 - Редактировать модуль");
            Console.WriteLine("| 4 - Удалить модуль\n|");
            Console.WriteLine("| 5 - Перейти к *Вопросам*\n|");
            Console.WriteLine("| 9 - Вернуться назад");
            Console.WriteLine("|\n+++++++++++++++++++++++++++++++++++++");
            Console.WriteLine("| Для выхода из программы нажмите q");
            Console.WriteLine("+++++++++++++++++++++++++++++++++++++");
            Console.Write("| Сделайте выбор: ");
            string menuID = Console.ReadLine();
            switch (menuID)
            {
                case "1":
                    {
                        Console.Clear();
                        modulsRequest.GetModuleRequest(subj_id);
                        Console.WriteLine("Для продолжения нажмите любую клавишу...");
                        Console.ReadKey();
                        LoadModulsMenu(subj_id);
                    }
                    break;
                case "2":
                    {
                        Console.Clear();
                        modulsRequest.AddModuleRequest(subj_id);
                        Console.WriteLine("Для продолжения нажмите любую клавишу...");
                        Console.ReadKey();
                        LoadModulsMenu(subj_id);
                    }
                    break;
                case "3":
                    {
                        Console.Clear();
                        modulsRequest.EditModuleRequest(subj_id);
                        Console.WriteLine("Для продолжения нажмите любую клавишу...");
                        Console.ReadKey();
                        LoadModulsMenu(subj_id);
                    }
                    break;
                case "4":
                    {
                        Console.Clear();
                        modulsRequest.RemoveModuleRequest(subj_id);
                        Console.WriteLine("Для продолжения нажмите любую клавишу...");
                        Console.ReadKey();
                        LoadModulsMenu(subj_id);
                    }
                    break;
                case "5":
                    {
                        try
                        {
                            Console.Clear();
                            modulsRequest.GetModuleRequest(subj_id);
                            Console.Write("Введите id модуля: ");
                            int modl_id = int.Parse(Console.ReadLine());
                            LoadQuestionsMenu(modl_id, subj_id);
                        }
                        catch (FormatException)
                        {
                            Console.WriteLine("Неправильный ввод! Введите id еще раз.");
                            Console.WriteLine("Для продолжения нажмите любую клавишу...");
                            Console.ReadKey();
                            LoadModulsMenu(subj_id);
                        }
                    }
                    break;
                case "9":
                    {
                        LoadSubjectsMenu();
                    }
                    break;
                case "q":
                    {
                        Environment.Exit(0);
                    }
                    break;
                default:
                    {
                        Console.Clear();
                        Console.WriteLine("Неправильный ввод! Введите номер меню еще раз.");
                        Console.WriteLine("Для продолжения нажмите любую клавишу...");
                        Console.ReadKey();
                        LoadModulsMenu(subj_id);
                    }
                    break;
            }
        }
        public static void LoadQuestionsMenu(int modl_id, int subj_id)
        {
            Console.Clear();
            QuestionRequests qsRequest = new QuestionRequests();
            Console.WriteLine("       *** Меню -Вопросы- ***");
            Console.WriteLine("+++++++++++++++++++++++++++++++++++++\n|");
            Console.WriteLine("| 1 - Просмотр вопросов");
            Console.WriteLine("| 2 - Добавить вопрос");
            Console.WriteLine("| 3 - Редактировать вопрос");
            Console.WriteLine("| 4 - Удалить вопрос \n|");
            Console.WriteLine("| 5 - Перейти к *Ответам*\n|");
            Console.WriteLine("| 9 - Вернуться назад");
            Console.WriteLine("|\n+++++++++++++++++++++++++++++++++++++");
            Console.WriteLine("| Для выхода из программы нажмите q");
            Console.WriteLine("+++++++++++++++++++++++++++++++++++++");
            Console.Write("| Сделайте выбор: ");
            string menuID = Console.ReadLine();
            switch (menuID)
            {
                case "1":
                    {
                        Console.Clear();
                        qsRequest.GetQuestionRequest(modl_id);
                        Console.WriteLine("Для продолжения нажмите любую клавишу...");
                        Console.ReadKey();
                        LoadQuestionsMenu(modl_id, subj_id);
                    }
                    break;
                case "2":
                    {
                        Console.Clear();
                        qsRequest.AddQuestionRequest(modl_id);
                        Console.WriteLine("Для продолжения нажмите любую клавишу...");
                        Console.ReadKey();
                        LoadQuestionsMenu(modl_id, subj_id);
                    }
                    break;
                case "3":
                    {
                        Console.Clear();
                        qsRequest.EditQuestionRequest(modl_id);
                        Console.WriteLine("Для продолжения нажмите любую клавишу...");
                        Console.ReadKey();
                        LoadQuestionsMenu(modl_id, subj_id);
                    }
                    break;
                case "4":
                    {
                        Console.Clear();
                        qsRequest.RemoveQuestionRequest(modl_id);
                        Console.WriteLine("Для продолжения нажмите любую клавишу...");
                        Console.ReadKey();
                        LoadQuestionsMenu(modl_id, subj_id);
                    }
                    break;
                case "5":
                    {
                        try
                        {
                            Console.Clear();
                            qsRequest.GetQuestionRequest(modl_id);
                            Console.Write("Введите id вопроса: ");
                            int qest_id = int.Parse(Console.ReadLine());
                            LoadAnswersMenu(qest_id, modl_id, subj_id);
                        }
                        catch (FormatException)
                        {
                            Console.WriteLine("Неправильный ввод! Введите id еще раз.");
                            Console.WriteLine("Для продолжения нажмите любую клавишу...");
                            Console.ReadKey();
                            LoadQuestionsMenu(modl_id, subj_id);
                        }
                    }
                    break;
                case "9":
                    {
                        LoadModulsMenu(subj_id);
                    }
                    break;
                case "q":
                    {
                        Environment.Exit(0);
                    }
                    break;
                default:
                    {
                        Console.Clear();
                        Console.WriteLine("Неправильный ввод! Введите номер меню еще раз.");
                        Console.WriteLine("Для продолжения нажмите любую клавишу...");
                        Console.ReadKey();
                        LoadQuestionsMenu(modl_id, subj_id);
                    }
                    break;
            }
        }
        public static void LoadAnswersMenu(int question_id, int modl_id, int subj_id)
        {
            Console.Clear();
            AnswerRequests answRequest = new AnswerRequests();
            Console.WriteLine("       *** Меню -Ответы- ***");
            Console.WriteLine("+++++++++++++++++++++++++++++++++++++\n|");
            Console.WriteLine("| 1 - Просмотр ответов");
            Console.WriteLine("| 2 - Добавить ответ");
            Console.WriteLine("| 3 - Редактировать ответ");
            Console.WriteLine("| 4 - Удалить ответ");
            Console.WriteLine("| 5 - Сопоставление ответов вопросу\n|");
            Console.WriteLine("| 9 - Вернуться назад");
            Console.WriteLine("|\n+++++++++++++++++++++++++++++++++++++");
            Console.WriteLine("| Для выхода из программы нажмите q");
            Console.WriteLine("+++++++++++++++++++++++++++++++++++++");
            Console.Write("| Сделайте выбор: ");
            string menuID = Console.ReadLine();
            switch (menuID)
            {
                case "1":
                    {
                        Console.Clear();
                        answRequest.GetAnswerRequest(question_id);
                        Console.WriteLine("Для продолжения нажмите любую клавишу...");
                        Console.ReadKey();
                        LoadAnswersMenu(question_id, modl_id, subj_id);
                    }
                    break;
                case "2":
                    {
                        Console.Clear();
                        answRequest.AddAnswerRequest(question_id);
                        Console.WriteLine("Для продолжения нажмите любую клавишу...");
                        Console.ReadKey();
                        LoadAnswersMenu(question_id, modl_id, subj_id);
                    }
                    break;
                case "3":
                    {
                        Console.Clear();
                        answRequest.EditAnswerRequest(question_id);
                        Console.WriteLine("Для продолжения нажмите любую клавишу...");
                        Console.ReadKey();
                        LoadAnswersMenu(question_id, modl_id, subj_id);
                    }
                    break;
                case "4":
                    {
                        Console.Clear();
                        answRequest.RemoveAnswerRequest(question_id);
                        Console.WriteLine("Для продолжения нажмите любую клавишу...");
                        Console.ReadKey();
                        LoadAnswersMenu(question_id, modl_id, subj_id);
                    }
                    break;
                case "5":
                    {
                        Console.Clear();
                        AnswersService answ = AnswersService.GetInstance();
                        AnswerDataModel anDM = new AnswerDataModel();
                        QuestionsService qs = QuestionsService.GetInstance();
                        QuestionDataModel qsDM = qs.GetData("ID", question_id).FirstOrDefault();
                        QuestionType qtype = qsDM.Type;

                        answRequest.GetAnswerRequest(question_id);
                        switch (qtype)
                        {
                            case QuestionType.OneAnswer:
                                {
                                    Console.WriteLine("Введите id правильного ответа");
                                    int curid = int.Parse(Console.ReadLine());
                                    anDM = answ.GetData("ID", curid).FirstOrDefault();
                                    anDM.Significance = 10;
                                    answ.SubminChanges();
                                }
                                break;
                            case QuestionType.MultipleAnswers:
                                {
                                    Console.WriteLine("Введите id правильных ответов через запятую");
                                    string[] curids = Console.ReadLine().Split(',');
                                    for (int i = 0; i < curids.Length; i++) {
                                        anDM = answ.GetData("ID", Convert.ToInt32(curids[i])).FirstOrDefault();
                                        anDM.Significance = (10 / curids.Length);
                                    }
                                    answ.SubminChanges();
                                }
                                break;
                            case QuestionType.Ordering:
                                {
                                    Console.WriteLine("Введите id в правильной последовательности, через запятую");
                                    string[] curids = Console.ReadLine().Split(',');
                                    for (int i = 0; i < curids.Length; i++)
                                    {
                                        anDM = answ.GetData("ID", Convert.ToInt32(curids[i])).FirstOrDefault();
                                        anDM.Significance = i+1;
                                    }
                                    answ.SubminChanges();
                                }
                                break;
                            case QuestionType.Classification:
                                {
                                    Console.WriteLine("Введите названия групп через запятую");
                                    string[] groups = Console.ReadLine().Split(',');
                                    for (int j = 0; j < groups.Length; j++)
                                    {
                                        Console.WriteLine("Введите через запятую id ответов, принадлежащие группе {0}",groups[j]);
                                        string[] curids = Console.ReadLine().Split(',');
                                        for (int i = 0; i < curids.Length; i++)
                                        {
                                            anDM = answ.GetData("ID", Convert.ToInt32(curids[i])).FirstOrDefault();
                                            anDM.Group = groups[j];
                                        }
                                    }
                                    answ.SubminChanges();
                                }
                                break;
                            case QuestionType.Compliance:
                                {
                                    Console.WriteLine("Введите пары id связных ответов через запятую (например: 1-3,2-4)");
                                    string[] groups = Console.ReadLine().Split(',');
                                    for (int j = 0; j < groups.Length; j++)
                                    {
                                        string[] curids = Console.ReadLine().Split('-');
                                        for (int i = 0; i < curids.Length; i++)
                                        {
                                            anDM = answ.GetData("ID", Convert.ToInt32(curids[i])).FirstOrDefault();
                                            anDM.Significance = j;
                                        }
                                    }
                                    answ.SubminChanges();
                                }
                                break;
                        }

                        Console.WriteLine("Для продолжения нажмите любую клавишу...");
                        Console.ReadKey();
                        LoadAnswersMenu(question_id, modl_id, subj_id);
                    }
                    break;
                case "9":
                    {
                        LoadQuestionsMenu(modl_id, subj_id);
                    }
                    break;
                case "q":
                    {
                        Environment.Exit(0);
                    }
                    break;
                default:
                    {
                        Console.Clear();
                        Console.WriteLine("Неправильный ввод! Введите номер меню еще раз.");
                        Console.WriteLine("Для продолжения нажмите любую клавишу...");
                        Console.ReadKey();
                        LoadAnswersMenu(question_id, modl_id, subj_id);
                    }
                    break;
            }
        }
    }
}
