﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace TeacherSubSystem
{
    public static class LoadXML
    {
        /// <summary>
        /// load XML-file
        /// </summary>
        /// <param name="XMLFile">Path to file</param>
        /// <returns>XDocument</returns>
        public static XDocument LoadXMLFile(string XMLFile)
        {
            try
            {
                XDocument xdoc = XDocument.Load(@"..\..\.."+XMLFile);
                return xdoc;
            }
            catch (System.IO.FileNotFoundException ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }
    }
}
