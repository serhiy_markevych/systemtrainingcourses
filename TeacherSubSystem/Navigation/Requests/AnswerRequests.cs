﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Configuration;
using CommonClassesLib.DataModels;
using CommonClassesLib.Services;

namespace TeacherSubSystem
{
    public class AnswerRequests
    {
        private XDocument _xmlDMLabels;
        private AnswerDataModel _Answer;
        private AnswersService _AnswerService;
        private QuestionsService _QuestionService;

        public AnswerRequests()
        {
            this._xmlDMLabels = LoadXML.LoadXMLFile(ConfigurationManager.ConnectionStrings["Path"].ConnectionString + "DMLabels.xml");
            this._Answer = new AnswerDataModel();
            this._AnswerService = AnswersService.GetInstance();
            this._QuestionService = QuestionsService.GetInstance();
        }

        public void GetAnswerRequest(int question_id)
        {
            QuestionRequests qr = new QuestionRequests();
            qr.GetQuestion(question_id);
            Console.WriteLine("Список ответов: ");
            Console.WriteLine("=====================================================================");
            foreach (var item in _AnswerService.GetData("QuestionsID", question_id))
            {
                Console.WriteLine("ID-ответа:{0} \nПорядковый номер:{1}", item.ID, item.OrdinalNumber);
                Console.WriteLine("Значимость:{0} \nГруппа:{1} \nПомечен на удаление:{2}", item.Significance, item.Group, item.CanceledMark);
                Console.WriteLine("Текст ответа:{0}", item.Text);
                Console.WriteLine("=====================================================================");
            }
        }
        public void AddAnswerRequest(int question_id)
        {
            try
            {
                string[] parms = new string[5];
                int count = 0;

                List<string> menu = new List<string>();
                var labels = from el in _xmlDMLabels.Root.Element("Answers").Element("Add").Elements()
                             select el;

                foreach (var item in labels)
                {
                    Console.Write(item.Value);
                    if (item.Attribute("name").Value == "questions_id")
                    {
                        Console.WriteLine("\n***");
                        List<QuestionDataModel> lq = _QuestionService.GetData();
                        foreach (var quest in lq) {
                            Console.WriteLine("ID: {0}\nName: {1}\n***", quest.ID, quest.Text);
                        }
                    }
                    parms[count++] = Console.ReadLine();
                }

                Console.WriteLine("Сохранить изминения?(Введите: 1=Да/0=Нет)");

                if (Console.ReadLine().Equals("1"))
                {
                    _Answer.QuestionsID = question_id;
                    _Answer.OrdinalNumber = Convert.ToInt32(parms[0]);
                    _Answer.Text = parms[1];
                    _Answer.Significance = 0;
                    _Answer.Group = "0";
                    _Answer.CanceledMark = Convert.ToInt32(parms[2]);
                    _AnswerService.AddData(_Answer);
                    _AnswerService.SubminChanges();
                    Console.WriteLine("Ответ успешно сохранен в базе даных");
                }
            }
            catch (FormatException)
            {
                Console.WriteLine("Введены некорректные данные!");
                Console.WriteLine("Данные не сохранены в базе даных");
            }
        }

        public void EditAnswerRequest(int question_id)
        {
            try
            {
                int answId = 0;
                GetAnswerRequest(question_id);

                Console.WriteLine("Введите id ответа для редактирования: ");

                answId = Convert.ToInt32(Console.ReadLine());

                if (answId != 0)
                {
                    string[] parms = new string[5];
                    int count = 0;

                    List<string> menu = new List<string>();
                    var labels = from el in _xmlDMLabels.Root.Element("Answers").Element("Edit").Elements()
                                 select el;

                    foreach (var item in labels)
                    {
                        Console.Write(item.Value);
                        parms[count++] = Console.ReadLine();
                    }

                    Console.WriteLine("Сохранить изминения?(Введите: 1=Да/0=Нет)");

                    if (Console.ReadLine().Equals("1"))
                    {
                        _Answer.QuestionsID = question_id;
                        _Answer.OrdinalNumber = Convert.ToInt32(parms[0]);
                        _Answer.Text = parms[1];
                        //_Answer.Significance = Convert.ToInt32(parms[2]);
                        //_Answer.Group = parms[3];
                        _Answer.CanceledMark = Convert.ToInt32(parms[2]);
                        _AnswerService.EditData(answId, _Answer);
                        _AnswerService.SubminChanges();
                        Console.WriteLine("Изменения успешно сохранены в базе даных");
                    }
                }
            }
            catch (FormatException)
            {
                Console.WriteLine("Введены некорректные данные!");
                Console.WriteLine("Ответ не сохранен в базе даных");
            }
        }
        public void RemoveAnswerRequest(int question_id)
        {
            int answId = 0;
            GetAnswerRequest(question_id);

            Console.WriteLine("Введите id ответа для удаления: ");

            try
            {
                answId = Convert.ToInt32(Console.ReadLine());
            }
            catch (FormatException)
            {
                Console.WriteLine("Введенно не корректный id предмета!");
            }

            if (answId != 0)
            {
                _AnswerService.RemoveData(answId);
                _AnswerService.SubminChanges();
                Console.WriteLine("Ответ удален");
            }
        }
    }
}
