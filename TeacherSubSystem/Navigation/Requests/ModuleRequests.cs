﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Configuration;
using CommonClassesLib.DataModels;
using CommonClassesLib.Services;

namespace TeacherSubSystem
{
    public class ModuleRequests
    {
        private XDocument _xmlDMLabels;
        private ModulDataModel _Module;
        private ModulsService _ModuleService;

        public ModuleRequests()
        {
            this._xmlDMLabels = LoadXML.LoadXMLFile(ConfigurationManager.ConnectionStrings["Path"].ConnectionString + "DMLabels.xml");
            this._Module = new ModulDataModel();
            this._ModuleService = ModulsService.GetInstance();
        }

        public void GetModuleRequest()
        {
            Console.WriteLine("\nСписок модулей: ");
            Console.WriteLine("==========================================================");
            foreach (var item in _ModuleService.GetData())
            {
                Console.WriteLine("ID-модуля:{0} \nНазвание:{1} ", item.ID, item.Name);
                Console.WriteLine("Помечен на удаление:{0} \nID-предмета:{1}", item.CanceledMark, item.SubjectsID);
                Console.WriteLine("==========================================================");
            }
        }
        public void GetModuleRequest(int subj_id)
        {
            Console.WriteLine("\nСписок модулей: ");
            Console.WriteLine("==========================================================");
            foreach (var item in _ModuleService.GetData("SubjectsID", subj_id))
            {
                Console.WriteLine("ID-модуля:{0} \nНазвание:{1} ", item.ID, item.Name);
                Console.WriteLine("Помечен на удаление:{0} \nID-предмета:{1}", item.CanceledMark, subj_id);
                Console.WriteLine("==========================================================");
            }
        }
        public void AddModuleRequest(int subj_id)
        {
            try
            {
                string[] parms = new string[2];
                int count = 0;

                List<string> menu = new List<string>();
                var labels = from el in _xmlDMLabels.Root.Element("Moduls").Element("Add").Elements()
                             select el;

                Console.WriteLine("*Добавление нового модуля*");

                foreach (var item in labels)
                {
                    Console.Write(item.Value);
                    parms[count++] = Console.ReadLine();
                }

                Console.WriteLine("Сохранить изминения?(Введите: 1=Да/0=Нет)");

                if (Console.ReadLine().Equals("1"))
                {
                    _Module.Name = parms[0];
                    _Module.CanceledMark = Convert.ToInt32(parms[1]);
                    _Module.SubjectsID = subj_id;
                    _ModuleService.AddData(_Module);
                    _ModuleService.SubminChanges();
                    Console.WriteLine("Модуль успешно сохранен в базе даных");
                }
            }
            catch (FormatException)
            {
                Console.WriteLine("Введены некорректные данные!");
                Console.WriteLine("Данные не сохранены в базе даных");
            }
        }
        public void EditModuleRequest(int subj_id)
        {
            try
            {
                int mdlId = 0;
                GetModuleRequest(subj_id);

                Console.WriteLine("Введите id модуля для редактирования: ");

                mdlId = Convert.ToInt32(Console.ReadLine());

                if (mdlId != 0)
                {
                    string[] parms = new string[2];
                    int count = 0;

                    List<string> menu = new List<string>();
                    var labels = from el in _xmlDMLabels.Root.Element("Moduls").Element("Edit").Elements()
                                 select el;

                    foreach (var item in labels)
                    {
                        Console.Write(item.Value);
                        parms[count++] = Console.ReadLine();
                    }

                    Console.WriteLine("Сохранить изминения?(Введите: 1=Да/0=Нет)");

                    if (Console.ReadLine().Equals("1"))
                    {
                        _Module.Name = parms[0];
                        _Module.CanceledMark = Convert.ToInt32(parms[1]);
                        _Module.SubjectsID = subj_id;
                        _ModuleService.EditData(mdlId, _Module);
                        _ModuleService.SubminChanges();
                        Console.WriteLine("Изменения успешно сохранены в базе даных");
                    }
                }
            }
            catch (FormatException)
            {
                Console.WriteLine("Введены некорректные данные!");
                Console.WriteLine("Данные не сохранены в базе даных");
            }
        }

        public void RemoveModuleRequest(int subj_id)
        {
            int mdlId = 0;
            GetModuleRequest(subj_id);

            Console.WriteLine("Введите id модуля для удаления: ");

            try
            {
                mdlId = Convert.ToInt32(Console.ReadLine());
            }
            catch (FormatException)
            {
                Console.WriteLine("Введенно не корректный id модуля!");
            }

            if (mdlId != 0)
            {
                _ModuleService.RemoveData(mdlId);
                _ModuleService.SubminChanges();
                Console.WriteLine("Модуль удален");
            }
        }
    }
}
