﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Configuration;
using CommonClassesLib.DataModels;
using CommonClassesLib.Services;

namespace TeacherSubSystem
{
    public class ProfileRequests
    {
        private XDocument _xmlDMLabels;
        private ProfileDataModel _Profile;
        private ProfilesService _ProfileService;

        public ProfileRequests()
        {
            this._xmlDMLabels = LoadXML.LoadXMLFile(ConfigurationManager.ConnectionStrings["Path"].ConnectionString + "DMLabels.xml");
            this._Profile = new ProfileDataModel();
            this._ProfileService = ProfilesService.GetInstance();
        }

        public void GetProfileRequest()
        {
            Console.WriteLine("\nСписок профилей: ");
            Console.WriteLine("==============================================================");
            foreach (var item in _ProfileService.GetData())
            {
                Console.WriteLine("ID-профиля:{0}; \nНазвание:{1}; \nПомечен на удаление:{2}; ", item.ID, item.Name, item.CanceledMark);
                Console.WriteLine("Лимит времени:{0}; \nПреремешивать вопросы:{1}; \n", item.TimeLimit, item.ShuffleQuestions);
                Console.WriteLine("Перемешивать ответы:{0}; \nК-во ответов:{1}; \nID-модуля:{2} ", item.ShuffleAnswers, item.QuestionsCount,
                    item.ModulsID);
                Console.WriteLine("==============================================================");
            }
        }
        public void AddProfileRequest()
        {
            try
            {
                string[] parms = new string[7];
                int count = 0;

                List<string> menu = new List<string>();
                var labels = from el in _xmlDMLabels.Root.Element("Profiles").Element("Add").Elements()
                             select el;

                Console.WriteLine("*Добавление нового профиля*");

                foreach (var item in labels)
                {
                    Console.Write(item.Value);
                    parms[count++] = Console.ReadLine();
                }

                Console.WriteLine("Сохранить изминения?(Введите: 1=Да/0=Нет)");

                if (Console.ReadLine().Equals("1"))
                {
                    _Profile.Name = parms[0];
                    _Profile.CanceledMark = Convert.ToInt32(parms[1]);
                    _Profile.TimeLimit = Convert.ToInt32(parms[2]);
                    _Profile.ShuffleQuestions = Convert.ToInt32(parms[3]);
                    _Profile.ShuffleAnswers = Convert.ToInt32(parms[4]);
                    _Profile.QuestionsCount = Convert.ToInt32(parms[5]);
                    _Profile.ModulsID = Convert.ToInt32(parms[6]);
                    _ProfileService.AddData(_Profile);
                    _ProfileService.SubminChanges();
                    Console.WriteLine("Профиль успешно сохранен в базе даных");
                }
            }
            catch (FormatException)
            {
                Console.WriteLine("Введены некорректные данные!");
                Console.WriteLine("Данные не сохранены в базе даных");
            }
        }
        public void EditProfileRequest()
        {
            try
            {
                int profId = 0;
                GetProfileRequest();

                Console.WriteLine("Введите id профиля для редактирования: ");

                profId = Convert.ToInt32(Console.ReadLine());

                if (profId != 0)
                {
                    string[] parms = new string[7];
                    int count = 0;

                    List<string> menu = new List<string>();
                    var labels = from el in _xmlDMLabels.Root.Element("Profiles").Element("Edit").Elements()
                                 select el;

                    foreach (var item in labels)
                    {
                        Console.Write(item.Value);
                        parms[count++] = Console.ReadLine();
                    }

                    Console.WriteLine("Сохранить изминения?(Введите: 1=Да/0=Нет)");

                    if (Console.ReadLine().Equals("1"))
                    {
                        _Profile.Name = parms[0];
                        _Profile.CanceledMark = Convert.ToInt32(parms[1]);
                        _Profile.TimeLimit = Convert.ToInt32(parms[2]);
                        _Profile.ShuffleQuestions = Convert.ToInt32(parms[3]);
                        _Profile.ShuffleAnswers = Convert.ToInt32(parms[4]);
                        _Profile.QuestionsCount = Convert.ToInt32(parms[5]);
                        _Profile.ModulsID = Convert.ToInt32(parms[6]);
                        _ProfileService.EditData(profId, _Profile);
                        _ProfileService.SubminChanges();
                        Console.WriteLine("Изменения успешно сохранены в базе даных");
                    }
                }
            }
            catch (FormatException)
            {
                Console.WriteLine("Введены некорректные данные!");
                Console.WriteLine("Данные не сохранены в базе даных");
            }
        }
        public void RemoveProfileRequest()
        {
            int profId = 0;
            GetProfileRequest();

            Console.WriteLine("Введите id профиля для удаления: ");

            try
            {
                profId = Convert.ToInt32(Console.ReadLine());
            }
            catch (FormatException)
            {
                Console.WriteLine("Введенно не корректный id профиля!");
            }

            if (profId != 0)
            {
                _ProfileService.RemoveData(profId);
                _ProfileService.SubminChanges();
                Console.WriteLine("Профиль удален");
            }
        }
    }
}
