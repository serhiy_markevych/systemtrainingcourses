﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Configuration;
using CommonClassesLib.DataModels;
using CommonClassesLib.Services;
using CommonClassesLib;

namespace TeacherSubSystem
{
    public class QuestionRequests
    {
        private XDocument _xmlDMLabels;
        private QuestionDataModel _Question;
        private QuestionsService _QuestionService;

        public QuestionRequests()
        {
            this._xmlDMLabels = LoadXML.LoadXMLFile(ConfigurationManager.ConnectionStrings["Path"].ConnectionString + "DMLabels.xml");
            this._Question = new QuestionDataModel();
            this._QuestionService = QuestionsService.GetInstance();
        }

        public void GetQuestionRequest(int modl_id)
        {
             Console.WriteLine("\nСписок вопросов: ");
            Console.WriteLine("==============================================================");
            foreach (var item in _QuestionService.GetData("ModulsID", modl_id))
            {
                Console.WriteLine("ID-вопроса:{0} \nПорядковый номер:{1};", item.ID, item.OrdinalNumber);
                Console.WriteLine("Текст вопроса:{0} ", item.Text);
                Console.WriteLine("Значимость:{0}; \nТип:{1}; \nID-модуля:{2}; Помечен на удаление:{3}",
                    item.Significance, item.Type, item.ModulsID, item.CanceledMark);
                Console.WriteLine("==============================================================");
            }
        }

        public void GetQuestion(int quest_id)
        {
            Console.WriteLine("\nВопрос: ");
            Console.WriteLine("==============================================================");
            foreach (var item in _QuestionService.GetData("ID", quest_id))
            {
                Console.WriteLine("ID:{0} Ordinal number:{1};", item.ID, item.OrdinalNumber);
                Console.WriteLine("Text:{0} ", item.Text);
                Console.WriteLine("Significance:{0}; Type:{1}; Moduls ID:{2}; Canceled mark:{3}",
                    item.Significance, item.Type, item.ModulsID, item.CanceledMark);
                Console.WriteLine("==============================================================");
            }
        }

        public void AddQuestionRequest(int modl_id)
        {
            try
            {
                string[] parms = new string[5];
                int count = 0;
                QuestionType qt = QuestionType.OneAnswer;

                List<string> menu = new List<string>();
                var labels = from el in _xmlDMLabels.Root.Element("Questions").Element("Add").Elements()
                             select el;

                Console.WriteLine("*Добавление нового вопроса*");

                foreach (var item in labels)
                {

                    if (item.Attribute("name").Value == "type")
                    {
                        Console.WriteLine("Введите id типа вопроса:");
                        Console.WriteLine(" 1 - OneAnswer");
                        Console.WriteLine(" 2 - MultipleAnswers");
                        Console.WriteLine(" 3 - Ordering");
                        Console.WriteLine(" 4 - Compliance");
                        Console.WriteLine(" 5 - Classification");
                        Console.WriteLine(" 6 - TypeAnswer");
                        switch (Console.ReadLine())
                        {
                            case "1":
                                qt = QuestionType.OneAnswer;
                                break;
                            case "2":
                                qt = QuestionType.MultipleAnswers;
                                break;
                            case "3":
                                qt = QuestionType.Ordering;
                                break;
                            case "4":
                                qt = QuestionType.Compliance;
                                break;
                            case "5":
                                qt = QuestionType.Classification;
                                break;
                            case "6":
                                qt = QuestionType.TypeAnswer;
                                break;
                            default:
                                Console.WriteLine("Выбран неверный id, повторите ввод");
                                AddQuestionRequest(modl_id);
                                break;
                        }
                        count++;
                    }
                    else
                    {
                        Console.Write(item.Value);
                        parms[count++] = Console.ReadLine();
                    }
                }

                Console.WriteLine("Сохранить изминения?(Введите: 1=Да/0=Нет)");

                if (Console.ReadLine().Equals("1"))
                {
                    _Question.OrdinalNumber = Convert.ToInt32(parms[0]);
                    _Question.Text = parms[1];
                    _Question.Significance = Convert.ToInt32(parms[2]);
                    _Question.Type = qt;
                    _Question.ModulsID = modl_id;
                    _Question.CanceledMark = Convert.ToInt32(parms[4]);
                    _QuestionService.AddData(_Question);
                    _QuestionService.SubminChanges();
                    Console.WriteLine("Вопрос сохранен в базе даных");
                }
            }
            catch (FormatException)
            {
                Console.WriteLine("Введены некорректные данные!");
                Console.WriteLine("Данные не сохранены в базе даных");
            }
        }
        public void EditQuestionRequest(int modl_id)
        {
            try
            {
                int quesId = 0;
                GetQuestionRequest(modl_id);
                QuestionType qt = QuestionType.OneAnswer;

                Console.WriteLine("Введите id вопроса для редактирования: ");

                quesId = Convert.ToInt32(Console.ReadLine());

                if (quesId != 0)
                {
                    string[] parms = new string[5];
                    int count = 0;

                    List<string> menu = new List<string>();
                    var labels = from el in _xmlDMLabels.Root.Element("Questions").Element("Edit").Elements()
                                 select el;

                    foreach (var item in labels)
                    {
                        if (item.Attribute("name").Value == "type")
                        {
                            Console.WriteLine("Введите id типа вопроса:");
                            Console.WriteLine(" 1 - OneAnswer");
                            Console.WriteLine(" 2 - MultipleAnswers");
                            Console.WriteLine(" 3 - Ordering");
                            Console.WriteLine(" 4 - Compliance");
                            Console.WriteLine(" 5 - Classification");
                            Console.WriteLine(" 6 - TypeAnswer");
                            switch (Console.ReadLine())
                            {
                                case "1":
                                    qt = QuestionType.OneAnswer;
                                    break;
                                case "2":
                                    qt = QuestionType.MultipleAnswers;
                                    break;
                                case "3":
                                    qt = QuestionType.Ordering;
                                    break;
                                case "4":
                                    qt = QuestionType.Compliance;
                                    break;
                                case "5":
                                    qt = QuestionType.Classification;
                                    break;
                                case "6":
                                    qt = QuestionType.TypeAnswer;
                                    break;
                                default:
                                    Console.WriteLine("Выбран неверный id, повторите ввод");
                                    AddQuestionRequest(modl_id);
                                    break;
                            }
                            count++;
                        }
                        else
                        {
                            Console.Write(item.Value);
                            parms[count++] = Console.ReadLine();
                        }
                    }

                    Console.WriteLine("Сохранить изминения?(Введите: 1=Да/0=Нет)");

                    if (Console.ReadLine().Equals("1"))
                    {
                        _Question.OrdinalNumber = Convert.ToInt32(parms[0]);
                        _Question.Text = parms[1];
                        _Question.Significance = Convert.ToInt32(parms[2]);
                        _Question.Type = qt;
                        _Question.ModulsID = modl_id;
                        _Question.CanceledMark = Convert.ToInt32(parms[4]);
                        _QuestionService.EditData(quesId, _Question);
                        _QuestionService.SubminChanges();
                        Console.WriteLine("Изменения успешно сохранены в базе даных");
                    }
                }
            }
            catch (FormatException)
            {
                Console.WriteLine("Введены некорректные данные!");
                Console.WriteLine("Данные не сохранены в базе даных");
            }
        }
        public void RemoveQuestionRequest(int modl_id)
        {
            int quesId = 0;
            GetQuestionRequest(modl_id);

            Console.WriteLine("Введите id вопроса для удаления: ");

            try
            {
                quesId = Convert.ToInt32(Console.ReadLine());
            }
            catch (FormatException)
            {
                Console.WriteLine("Введенно не корректный id вопроса!");
            }

            if (quesId != 0)
            {
                _QuestionService.RemoveData(quesId);
                _QuestionService.SubminChanges();
                Console.WriteLine("Вопрос удален");
            }
        }
    }
}
