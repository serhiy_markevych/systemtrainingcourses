﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Configuration;
using CommonClassesLib.DataModels;
using CommonClassesLib.Services;

namespace TeacherSubSystem
{
    public class RatingScaleRequests
    {
        private XDocument _xmlDMLabels;
        private RatingScaleDataModel _RatingScale;
        private RatingScaleService _RatingScaleService;

        public RatingScaleRequests()
        {
            this._xmlDMLabels = LoadXML.LoadXMLFile(ConfigurationManager.ConnectionStrings["Path"].ConnectionString + "DMLabels.xml");
            this._RatingScale = new RatingScaleDataModel();
            this._RatingScaleService = RatingScaleService.GetInstance();
        }

        public void GetRatingScaleRequest(int prof_id)
        {
            Console.WriteLine("\nСписок шкалы оценок: ");
            Console.WriteLine("==============================================================");
            foreach (var item in _RatingScaleService.GetData("ProfilesID", prof_id))
            {
                Console.WriteLine("ID-шкалы оценок{0}; \nID-профиля:{1}", item.ID, item.ProfilesID);
                Console.WriteLine("Подпись:{0} ", item.Text);
                Console.WriteLine("нижняя граница:{0}", item.LowBound);
                Console.WriteLine("==============================================================");
            }
        }
        public void AddRatingScaleRequest(int prof_id)
        {
            try
            {
                string[] parms = new string[2];
                int count = 0;

                List<string> menu = new List<string>();
                var labels = from el in _xmlDMLabels.Root.Element("RatingScale").Element("Add").Elements()
                             select el;

                Console.WriteLine("*Добавление новой шкалы оценок*");

                foreach (var item in labels)
                {
                    Console.Write(item.Value);
                    parms[count++] = Console.ReadLine();
                }

                Console.WriteLine("Сохранить изминения?(Введите: 1=Да/0=Нет)");

                if (Console.ReadLine().Equals("1"))
                {
                    _RatingScale.ProfilesID = prof_id;
                    _RatingScale.Text = parms[0];
                    _RatingScale.LowBound = Convert.ToInt32(parms[1]);
                    _RatingScaleService.AddData(_RatingScale);
                    _RatingScaleService.SubminChanges();
                    Console.WriteLine("Шкала оценок сохранена в базе даных");
                }
            }
            catch (FormatException)
            {
                Console.WriteLine("Введены некорректные данные!");
                Console.WriteLine("Данные не сохранены в базе даных");
            }
        }
        public void EditRatingScaleRequest(int prof_id)
        {
            try
            {
                int rsId = 0;
                GetRatingScaleRequest(prof_id);

                Console.WriteLine("Введите id шкалы оценок для редактирования: ");

                rsId = Convert.ToInt32(Console.ReadLine());

                if (rsId != 0)
                {
                    string[] parms = new string[2];
                    int count = 0;

                    List<string> menu = new List<string>();
                    var labels = from el in _xmlDMLabels.Root.Element("RatingScale").Element("Edit").Elements()
                                 select el;

                    foreach (var item in labels)
                    {
                        Console.Write(item.Value);
                        parms[count++] = Console.ReadLine();
                    }

                    Console.WriteLine("Сохранить изминения?(Введите: 1=Да/0=Нет)");

                    if (Console.ReadLine().Equals("1"))
                    {
                        _RatingScale.ProfilesID = prof_id;
                        _RatingScale.Text = parms[0];
                        _RatingScale.LowBound = Convert.ToInt32(parms[1]);
                        _RatingScaleService.EditData(rsId, _RatingScale);
                        _RatingScaleService.SubminChanges();
                        Console.WriteLine("Изменения успешно сохранены в базе даных");
                    }
                }
            }
            catch (FormatException)
            {
                Console.WriteLine("Введены некорректные данные!");
                Console.WriteLine("Данные не сохранены в базе даных");
            }
        }
        public void RemoveRatingScaleRequest(int prof_id)
        {
            int rsId = 0;
            GetRatingScaleRequest(prof_id);

            Console.WriteLine("Введите id шкалы оценко для удаления: ");

            try
            {
                rsId = Convert.ToInt32(Console.ReadLine());
            }
            catch (FormatException)
            {
                Console.WriteLine("Введенно не корректный id шкалы оценок!");
            }

            if (rsId != 0)
            {
                _RatingScaleService.RemoveData(rsId);
                _RatingScaleService.SubminChanges();
                Console.WriteLine("Шкала оценок удалена");
            }
        }
    }
}
