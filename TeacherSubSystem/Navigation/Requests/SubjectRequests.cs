﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Configuration;
using CommonClassesLib.DataModels;
using CommonClassesLib.Services;

namespace TeacherSubSystem
{
    public class SubjectRequests
    {
        private XDocument _xmlDMLabels;
        private SubjectDataModel _subject;
        private SubjectsService _subjectService;

        public SubjectRequests()
        {
            this._xmlDMLabels = LoadXML.LoadXMLFile(ConfigurationManager.ConnectionStrings["Path"].ConnectionString + "DMLabels.xml");
            this._subject = new SubjectDataModel();
            this._subjectService = SubjectsService.GetInstance();
        }

        public void GetSubjectRequest()
        {
            Console.WriteLine("Список всех предметов: ");
            
            foreach (var item in _subjectService.GetData())
            {
                Console.WriteLine("-----------------------------------------------------------------------");
                Console.WriteLine("ID-предмета: {0} \nНазвание: {1} \nПомечен на удаление: {2}", item.ID, item.Name, item.CanceledMark);
            }
            Console.WriteLine("-----------------------------------------------------------------------");
        }
        public void AddSubjectRequest()
        {
            try
            {
                string[] parms = new string[2];
                int count = 0;

                List<string> menu = new List<string>();
                var labels = from el in _xmlDMLabels.Root.Element("Subjects").Element("Add").Elements()
                             select el;
                Console.WriteLine("*** Добавление нового предмета ***");
                foreach (var item in labels)
                {
                    Console.Write(item.Value);
                    parms[count++] = Console.ReadLine();
                }

                Console.WriteLine("Сохранить изминения?(Введите: 1=Да/0=Нет)");

                if (Console.ReadLine().Equals("1"))
                {
                    _subject.Name = parms[0].ToString();
                    _subject.CanceledMark = Convert.ToInt32(parms[1]);
                    _subjectService.AddData(_subject);
                    _subjectService.SubminChanges();
                    Console.WriteLine("Предмет успешно сохранен в базе даных");
                }

                Console.WriteLine("Укажите профайл для предмета");
                ProfileRequests pr = new ProfileRequests();
                pr.GetProfileRequest();
                int profile_id = int.Parse(Console.ReadLine());
                AssignDataModel _Assign = new AssignDataModel();
                AssignService _AssignService = AssignService.GetInstance();

                int maxid = (from item in _subjectService.GetData()
                            orderby item.ID descending
                            select item.ID).FirstOrDefault();

                _Assign.SubjectsID = maxid;
                _Assign.GroupsID = 0;
                _Assign.TeacherID = DisplayMenu.UserID;
                _Assign.ProfilesID = profile_id;

                _AssignService.AddData(_Assign);
                _AssignService.SubminChanges();

            }
            catch (FormatException)
            {
                Console.WriteLine("Введены некорректные данные!");
                Console.WriteLine("Данные не сохранены в базе даных");
            }
        }

        public void EditSubjectRequest()
        {
            try
            {
                int subId = 0;
                Console.WriteLine("Список предметов:");

                GetSubjectRequest();

                Console.WriteLine("Введите id предмета для редактирования: ");

                subId = Convert.ToInt32(Console.ReadLine());

                if (subId != 0)
                {
                    string[] parms = new string[2];
                    int count = 0;

                    List<string> menu = new List<string>();
                    var labels = from el in _xmlDMLabels.Root.Element("Subjects").Element("Edit").Elements()
                                 select el;

                    foreach (var item in labels)
                    {
                        Console.Write(item.Value);
                        parms[count++] = Console.ReadLine();
                    }

                    Console.WriteLine("Сохранить изминения?(Введите: 1=Да/0=Нет)");

                    if (Console.ReadLine().Equals("1"))
                    {
                        _subject.Name = parms[0].ToString();
                        _subject.CanceledMark = Convert.ToInt32(parms[1]);
                        _subjectService.EditData(subId, _subject);
                        _subjectService.SubminChanges();
                        Console.WriteLine("Изменения успешно сохранены в базе даных");
                    }
                }
            }
            catch (FormatException)
            {
                Console.WriteLine("Введены некорректные данные!");
                Console.WriteLine("Данные не сохранены в базе даных");
            }
        }
        public void RemoveSubjectRequest()
        {
            int subId = 0;
            GetSubjectRequest();

            Console.WriteLine("Введите id предмета для удаления: ");

            try
            {
                subId = Convert.ToInt32(Console.ReadLine());
            }
            catch (FormatException)
            {
                Console.WriteLine("Введенно не корректный id предмета!");
            }

            if (subId != 0)
            {
                _subjectService.RemoveData(subId);
                _subjectService.SubminChanges();
                Console.WriteLine("Предмет удален");
            }
        }
    }
}
